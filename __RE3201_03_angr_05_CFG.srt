﻿1
00:00:00,149 --> 00:00:03,620
Let's start with the next challenge.

2
00:00:03,620 --> 00:00:05,619
That's the crackme number four.

3
00:00:05,644 --> 00:00:11,219
That is also in the same folder,
the other challenges.

4
00:00:13,159 --> 00:00:16,406
We just loaded my llbp

5
00:00:17,259 --> 00:00:18,465
And then, I will check with file,

6
00:00:18,490 --> 00:00:20,400
which kind of binary I have.

7
00:00:20,400 --> 00:00:24,080
It's again an ELF binary, 32 bit.

8
00:00:24,080 --> 00:00:26,075
So, let's check the strings.

9
00:00:26,100 --> 00:00:30,349
In the strings, this time,
I have it back, the password okay,

10
00:00:30,350 --> 00:00:32,979
the password incorrect strings.

11
00:00:33,004 --> 00:00:36,371
I also have the IOLI crackme string.

12
00:00:36,372 --> 00:00:40,154
And the structure of the strings
is pretty much the same.

13
00:00:42,077 --> 00:00:45,892
We'll just load the binary into lldb.

14
00:00:45,892 --> 00:00:47,826
And disassemble the main.

15
00:00:47,851 --> 00:00:53,264
In the main function, we can see that
the structure also didn't change much.

16
00:00:53,289 --> 00:00:56,850
We have to printfs
followed by a scanf,

17
00:00:56,850 --> 00:01:01,875
but this time, we also don't have
compare directly, the main function,

18
00:01:02,155 --> 00:01:05,120
but we have a call to another function,

19
00:01:05,145 --> 00:01:06,602
the check function,

20
00:01:06,627 --> 00:01:11,690
So, we'll check this check function,

21
00:01:11,691 --> 00:01:17,631
and we'll see that we
have the string call,

22
00:01:17,631 --> 00:01:19,961
and then, we have a compare.

23
00:01:19,961 --> 00:01:22,123
And then, we'll
scroll a little bit,

24
00:01:22,148 --> 00:01:28,130
and we'll see that we have a
scanf call and another compare.

25
00:01:28,131 --> 00:01:31,075
And then, we finally have
a printf with an exit,

26
00:01:31,100 --> 00:01:36,060
and another printf returning
to the main function.

27
00:01:36,060 --> 00:01:38,380
So, it seems pretty similar, right?

28
00:01:38,380 --> 00:01:46,656
We'll check, but we are going to
print in this list two printf, one is in the 3b.

29
00:01:46,681 --> 00:01:49,560
That's the one we want.

30
00:01:49,560 --> 00:01:52,890
So, we know that we want to exit here,

31
00:01:52,890 --> 00:01:56,405
but let's continue,

32
00:01:56,430 --> 00:02:02,440
We are going to try the same approach that
we did before with the other two binaries.

33
00:02:02,440 --> 00:02:04,294
So, import angr.

34
00:02:05,153 --> 00:02:10,656
We'll load the binary and
we start the simulation.

35
00:02:10,967 --> 00:02:19,099
We are going to try to reach the
branching point like we did before.

36
00:02:19,100 --> 00:02:22,108
Angr finds two active paths again.

37
00:02:22,133 --> 00:02:25,460
We'll check the kind of input we need for it

38
00:02:25,460 --> 00:02:28,220
But this time, we'll just get garbage.

39
00:02:28,220 --> 00:02:30,100
So, what happened?

40
00:02:30,125 --> 00:02:33,645
If we look into the
check function again,

41
00:02:34,153 --> 00:02:36,414
you're going to see that this time,

42
00:02:36,439 --> 00:02:39,618
our function is not as
linear as they were before.

43
00:02:39,643 --> 00:02:43,209
The order to crackme is
that we solved it before.

44
00:02:43,234 --> 00:02:45,721
We just had one branch point,

45
00:02:45,746 --> 00:02:49,236
exactly the last one that we have here.

46
00:02:49,261 --> 00:02:54,651
Compare, and then, a jump to the
wrong one or the right one,

47
00:02:54,651 --> 00:02:56,550
and that was it.

48
00:02:56,550 --> 00:03:00,111
But this time, we had a compare before.

49
00:03:00,111 --> 00:03:06,080
So, we have more than one
branching point in this binary.

50
00:03:06,907 --> 00:03:14,651
What we need to do is tell angr what
we really want to find in this binary.

51
00:03:14,668 --> 00:03:17,891
We cannot just run to the
next branching point.

52
00:03:17,891 --> 00:03:20,740
You could but you can do it better.

53
00:03:20,740 --> 00:03:25,352
And the way of doing it
a little bit better is,

54
00:03:25,377 --> 00:03:30,341
let's check this second printf
that we didn't check before.

55
00:03:30,341 --> 00:03:33,201
This is the password incorrect,

56
00:03:33,201 --> 00:03:42,850
and remember that the one before,
it was the password correct.

57
00:03:42,850 --> 00:03:48,153
So, what we are going to
store is an 'avoid address,'

58
00:03:48,406 --> 00:03:52,249
an address that we don't
want to reach with angr,

59
00:03:52,250 --> 00:03:56,731
and this would be this address here.

60
00:03:56,731 --> 00:04:01,320
This is where we are going
to jump if it's wrong.

61
00:04:01,320 --> 00:04:02,893
If the compare fails,

62
00:04:02,918 --> 00:04:07,405
we are going to jump here to store
the password incorrect string

63
00:04:07,406 --> 00:04:09,431
and printf right after.

64
00:04:10,520 --> 00:04:13,811
So, we want to avoid coming
to this line of the code.

65
00:04:13,811 --> 00:04:18,860
We'll just store it in a variable.

66
00:04:18,860 --> 00:04:26,820
And what angr really need from us for finding
the right path is a kind of fun map.

67
00:04:26,820 --> 00:04:30,893
So, this map is called
control flow graph

68
00:04:30,905 --> 00:04:34,990
where you have a graph
with all the functions

69
00:04:34,990 --> 00:04:39,266
and options that data can go from the
beginning of the binary to the end,

70
00:04:39,292 --> 00:04:44,380
and we are going to try to
generate this control flow graph.

71
00:04:44,380 --> 00:04:49,655
As you can see here, we cannot generate
this kind of control flow graph

72
00:04:51,154 --> 00:04:54,610
when we are loading
the libraries automatically.

73
00:05:00,904 --> 00:05:07,572
Let me just stop this cell because
it's going to kill the computer.

74
00:05:07,597 --> 00:05:11,012
We need to reload the binary into angr.

75
00:05:11,037 --> 00:05:15,089
Deactivating the auto_load_libs,

76
00:05:15,114 --> 00:05:20,362
and the way to do this is pretty simple.

77
00:05:22,409 --> 00:05:25,319
We are going to add this option

78
00:05:25,838 --> 00:05:30,189
when we are loading the
binary with the project call.

79
00:05:31,289 --> 00:05:35,029
As you remember, in the beginning,
we always do project, angr, project,

80
00:05:35,535 --> 00:05:38,940
and then, we create it with the binary.

81
00:05:38,941 --> 00:05:41,580
What we need to add now
is these load options,

82
00:05:41,605 --> 00:05:45,771
and we'll turn the
auto_load_libs to false.

83
00:05:45,771 --> 00:05:51,841
That's all we need to do to be able to
create a control flow graph of this binary.

84
00:05:51,841 --> 00:05:59,611
So, we overrode the project now
with these loading options.

85
00:05:59,611 --> 00:06:02,041
And now, we can create
the control flow graph.

86
00:06:05,491 --> 00:06:11,420
To create the control flow graph,
we are going to use the analysis,

87
00:06:11,420 --> 00:06:14,039
and then, we have the option CFG.

88
00:06:17,211 --> 00:06:19,551
As you can see, it generated,
and it's pretty fast,

89
00:06:19,576 --> 00:06:22,370
because the binary is very small.

90
00:06:22,371 --> 00:06:30,010
Another feature that we have on angr
is a knowledge base of functions.

91
00:06:30,010 --> 00:06:34,385
And as our binary
is not stripped.

92
00:06:34,410 --> 00:06:37,347
We have a table of symbols.

93
00:06:37,372 --> 00:06:42,241
That is knowledge base in angr.

94
00:06:42,242 --> 00:06:46,218
So, we can search the address
of a function just by the name.

95
00:06:46,243 --> 00:06:48,187
We don't need to look

96
00:06:48,212 --> 00:06:52,932
in which address are we going to
call it function or anything,

97
00:06:52,932 --> 00:07:02,250
because if it's dynamically, or if
angr is changing the base address

98
00:07:02,275 --> 00:07:03,783
where it's loading the binary.

99
00:07:03,808 --> 00:07:08,441
Sometimes, it can happen,
and then, it changed the address.

100
00:07:08,442 --> 00:07:13,481
So this way, it's cleaner because
you're not like hardcoding

101
00:07:13,482 --> 00:07:16,023
all the addresses, right?

102
00:07:16,023 --> 00:07:21,170
If we can remember from
the check function,

103
00:07:21,195 --> 00:07:27,789
or one of the functions that we wanted to
call after printf was the exit function

104
00:07:27,829 --> 00:07:30,936
because this function was just
called after the success, right?

105
00:07:34,320 --> 00:07:42,449
So, we are going to find the address of the
function named exit in our knowledge base

106
00:07:42,449 --> 00:07:44,669
of the control flow graph.

107
00:07:44,669 --> 00:07:53,020
As I said, the control flow graph
is really like a map for our binary.

108
00:07:53,020 --> 00:07:57,349
And you have an index where you
can search for the street names,

109
00:07:57,374 --> 00:08:02,189
for example, as we search for the
function names and its addresses.

110
00:08:02,189 --> 00:08:06,720
What we are going to do
is exactly the same.

111
00:08:06,745 --> 00:08:09,780
We are creating just a
simulation manager this time,

112
00:08:09,781 --> 00:08:15,125
and we are going to use the
simulation manager function explore.

113
00:08:15,150 --> 00:08:21,643
With the explore, what angr is
going to do is try to find address,

114
00:08:21,655 --> 00:08:24,584
trying to reach the exit function.

115
00:08:24,609 --> 00:08:30,779
In this case, and it's going to avoid
the avoid address that we just stored.

116
00:08:30,779 --> 00:08:34,840
That is the password not okay.

117
00:08:34,840 --> 00:08:40,303
So, when we run this,
angr is going to do it by itself.

118
00:08:40,318 --> 00:08:42,840
We don't need to do much.

119
00:08:42,840 --> 00:08:49,830
And what happened is that the simulation
manager found two branching points,

120
00:08:49,830 --> 00:08:58,710
and one of the paths that it has now
in its database is a found.

121
00:08:58,710 --> 00:09:05,887
That means we have a path
that lead us to the exit,

122
00:09:05,912 --> 00:09:12,520
or to rhe find the address and
it avoided two other paths.

123
00:09:12,520 --> 00:09:17,537
So, let's check what's
in the find address.

124
00:09:23,693 --> 00:09:30,910
as you can see, we have here
a number like eighty-seven.

125
00:09:30,910 --> 00:09:38,180
So, we are going to restore the
eighty-seven in her flag and test it.

126
00:09:38,180 --> 00:09:39,490
And there you go.

127
00:09:39,490 --> 00:09:41,290
This is our flag.

128
00:09:41,290 --> 00:09:46,884
I just want to give a
notice about this crackme.

129
00:09:46,909 --> 00:09:50,030
It has multiple solutions.

130
00:09:50,031 --> 00:09:58,604
So, if the solution that you get from
angr doesn't work into the OST platform,

131
00:09:58,629 --> 00:10:01,486
please, let me know.

