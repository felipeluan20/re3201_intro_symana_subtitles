1
00:00:00,160 --> 00:00:06,480
so let's solve the challenge moon from

2
00:00:03,040 --> 00:00:08,000
the shakti teaser event or using what we

3
00:00:06,480 --> 00:00:08,880
just learned

4
00:00:08,000 --> 00:00:10,719
first

5
00:00:08,880 --> 00:00:12,639
i will run the challenge

6
00:00:10,719 --> 00:00:14,080
just to see

7
00:00:12,639 --> 00:00:16,880
what happens

8
00:00:14,080 --> 00:00:18,160
so it brings the string hello there

9
00:00:16,880 --> 00:00:20,080
and

10
00:00:18,160 --> 00:00:21,359
asks for the input of the correct

11
00:00:20,080 --> 00:00:23,439
flagrant

12
00:00:21,359 --> 00:00:26,680
so

13
00:00:23,439 --> 00:00:26,680
let's see

14
00:00:32,239 --> 00:00:35,920
and then it fails with the extreme long

15
00:00:34,320 --> 00:00:38,320
way to go

16
00:00:35,920 --> 00:00:40,800
um but it's interesting to see that i

17
00:00:38,320 --> 00:00:44,640
needed to input two

18
00:00:40,800 --> 00:00:47,920
pieces of information before it checks

19
00:00:44,640 --> 00:00:47,920
so let's go back

20
00:00:48,399 --> 00:00:56,079
first we set up our environment

21
00:00:51,760 --> 00:00:56,079
as we already did in the other

22
00:00:56,399 --> 00:00:58,800
challenge

23
00:00:57,520 --> 00:01:02,840
and now

24
00:00:58,800 --> 00:01:02,840
let's check these things

25
00:01:22,080 --> 00:01:27,159
so we see it's using a scanf

26
00:01:27,200 --> 00:01:31,520
and here is

27
00:01:29,439 --> 00:01:35,200
this thing that we saw just in the

28
00:01:31,520 --> 00:01:36,799
beginning and right after we see the

29
00:01:35,200 --> 00:01:39,600
string that

30
00:01:36,799 --> 00:01:42,079
totals that we faked

31
00:01:39,600 --> 00:01:44,399
we have a fake string here

32
00:01:42,079 --> 00:01:47,200
and it's also interesting to see he is

33
00:01:44,399 --> 00:01:50,399
your flag and then we have a

34
00:01:47,200 --> 00:01:52,880
string format with two s like expecting

35
00:01:50,399 --> 00:01:53,759
to be strings or printing to be strings

36
00:01:52,880 --> 00:01:55,920
so

37
00:01:53,759 --> 00:01:59,360
maybe when we

38
00:01:55,920 --> 00:02:01,439
in when we input our two

39
00:01:59,360 --> 00:02:03,840
strings like before

40
00:02:01,439 --> 00:02:06,840
uh it's going to be printed in the in

41
00:02:03,840 --> 00:02:09,280
the end with well

42
00:02:06,840 --> 00:02:11,840
done and

43
00:02:09,280 --> 00:02:13,040
what we want probably is like the well

44
00:02:11,840 --> 00:02:16,800
done

45
00:02:13,040 --> 00:02:16,800
and the here's your flag

46
00:02:17,040 --> 00:02:23,840
so let's

47
00:02:19,840 --> 00:02:23,840
check it under the poker

48
00:02:29,200 --> 00:02:34,480
so here we go

49
00:02:30,640 --> 00:02:34,480
what we see here is

50
00:02:34,640 --> 00:02:39,920
a boots and here we see there is conf

51
00:02:38,480 --> 00:02:41,040
call

52
00:02:39,920 --> 00:02:44,400
with

53
00:02:41,040 --> 00:02:46,879
some kind of

54
00:02:44,400 --> 00:02:49,840
string format

55
00:02:46,879 --> 00:02:52,239
we can also see here that we

56
00:02:49,840 --> 00:02:54,400
are storing the

57
00:02:52,239 --> 00:02:57,760
these kind of results

58
00:02:54,400 --> 00:03:03,120
here on this tag

59
00:02:57,760 --> 00:03:03,120
and what we are going to do is check the

60
00:03:03,280 --> 00:03:08,879
testing format to be sure

61
00:03:05,599 --> 00:03:08,879
so we can do that

62
00:03:13,040 --> 00:03:17,879
in another

63
00:03:14,879 --> 00:03:17,879
cell

64
00:03:22,959 --> 00:03:28,080
i'm going to check this string format

65
00:03:26,000 --> 00:03:30,840
and

66
00:03:28,080 --> 00:03:34,239
what we have here

67
00:03:30,840 --> 00:03:35,480
is 873 hex

68
00:03:34,239 --> 00:03:38,159
plus

69
00:03:35,480 --> 00:03:41,200
348 a

70
00:03:38,159 --> 00:03:41,200
so let's do that

71
00:03:42,000 --> 00:03:48,400
eight seven

72
00:03:44,560 --> 00:03:48,400
three close

73
00:03:48,799 --> 00:03:51,840
four eight a

74
00:03:53,920 --> 00:03:57,200
and then we see the

75
00:03:56,000 --> 00:03:59,680
uh

76
00:03:57,200 --> 00:04:01,200
that it's the scanner is expecting to be

77
00:03:59,680 --> 00:04:05,040
strings

78
00:04:01,200 --> 00:04:08,560
so it's what the same behavior we

79
00:04:05,040 --> 00:04:12,840
expected already from running the binary

80
00:04:08,560 --> 00:04:15,760
so we continue looking into the this

81
00:04:12,840 --> 00:04:17,919
angle a little bit and then we see two

82
00:04:15,760 --> 00:04:20,160
calls for a string length

83
00:04:17,919 --> 00:04:22,479
and it's measuring the length of the

84
00:04:20,160 --> 00:04:25,759
input that we have

85
00:04:22,479 --> 00:04:26,800
from this kind of or from or

86
00:04:25,759 --> 00:04:29,440
flex

87
00:04:26,800 --> 00:04:32,560
and they are both failing somewhere else

88
00:04:29,440 --> 00:04:33,600
how if it's not equal

89
00:04:32,560 --> 00:04:36,080
um

90
00:04:33,600 --> 00:04:37,919
it's going to be

91
00:04:36,080 --> 00:04:39,759
jumping to

92
00:04:37,919 --> 00:04:41,120
8 a

93
00:04:39,759 --> 00:04:43,280
1

94
00:04:41,120 --> 00:04:47,040
ha1

95
00:04:43,280 --> 00:04:49,840
comes to a call for puts and exit

96
00:04:47,040 --> 00:04:50,639
so we could assume that it's

97
00:04:49,840 --> 00:04:51,600
uh

98
00:04:50,639 --> 00:04:52,880
bad

99
00:04:51,600 --> 00:04:56,000
because it's

100
00:04:52,880 --> 00:04:57,919
x exiting before

101
00:04:56,000 --> 00:05:01,039
doing anything else

102
00:04:57,919 --> 00:05:04,639
and then the next string of length

103
00:05:01,039 --> 00:05:05,919
is going to hp7

104
00:05:04,639 --> 00:05:08,840
and

105
00:05:05,919 --> 00:05:12,160
hb7

106
00:05:08,840 --> 00:05:14,639
is um if it's equal

107
00:05:12,160 --> 00:05:15,759
it's going to start the manipulation of

108
00:05:14,639 --> 00:05:18,720
this string

109
00:05:15,759 --> 00:05:20,800
so we should assume that um both of our

110
00:05:18,720 --> 00:05:23,360
a strings should

111
00:05:20,800 --> 00:05:26,639
have a length of 17.

112
00:05:23,360 --> 00:05:28,720
so hex 11.

113
00:05:26,639 --> 00:05:32,400
so we go down a little bit more and we

114
00:05:28,720 --> 00:05:34,720
see that we have a lot of

115
00:05:32,400 --> 00:05:37,759
information lost

116
00:05:34,720 --> 00:05:41,039
because uh with the manipulation here so

117
00:05:37,759 --> 00:05:42,080
it's not really possible to reverse

118
00:05:41,039 --> 00:05:44,479
in a

119
00:05:42,080 --> 00:05:48,800
traditional way

120
00:05:44,479 --> 00:05:48,800
and we keep going down and down and down

121
00:05:50,160 --> 00:05:55,039
and looking for compares or other

122
00:05:53,199 --> 00:05:58,080
branching points

123
00:05:55,039 --> 00:06:00,479
that are important and here we are

124
00:05:58,080 --> 00:06:02,919
that's the first compare and if it's not

125
00:06:00,479 --> 00:06:06,720
equal let's jump into

126
00:06:02,919 --> 00:06:08,720
be2 let's see what it's in there

127
00:06:06,720 --> 00:06:11,600
be2 is

128
00:06:08,720 --> 00:06:12,560
puts and exit so it's probably not good

129
00:06:11,600 --> 00:06:15,919
again

130
00:06:12,560 --> 00:06:18,160
but we can check what is the string

131
00:06:15,919 --> 00:06:21,400
that it's printing

132
00:06:18,160 --> 00:06:21,400
right there

133
00:06:28,479 --> 00:06:33,160
and it was

134
00:06:30,160 --> 00:06:33,160
in

135
00:06:37,600 --> 00:06:40,720
feet in nine

136
00:06:45,360 --> 00:06:49,759
three

137
00:06:46,840 --> 00:06:53,440
four and we can see it's printing not

138
00:06:49,759 --> 00:06:54,400
there yet so it's it's one of the

139
00:06:53,440 --> 00:06:56,400
fair

140
00:06:54,400 --> 00:06:59,520
strings

141
00:06:56,400 --> 00:07:01,919
so we can move further

142
00:06:59,520 --> 00:07:04,639
on the code and we see another compare

143
00:07:01,919 --> 00:07:07,280
with a jump not equal to the same

144
00:07:04,639 --> 00:07:08,479
address be2

145
00:07:07,280 --> 00:07:11,440
and then

146
00:07:08,479 --> 00:07:13,680
it continues until a

147
00:07:11,440 --> 00:07:16,720
new string conquer

148
00:07:13,680 --> 00:07:20,080
with a test if it's equal it's to if

149
00:07:16,720 --> 00:07:22,560
it's not equal it's jumping to bcc

150
00:07:20,080 --> 00:07:24,960
so let's see what's in bcc

151
00:07:22,560 --> 00:07:28,080
it's another put so let's check what

152
00:07:24,960 --> 00:07:31,080
it's printing that

153
00:07:28,080 --> 00:07:31,080
line

154
00:07:38,720 --> 00:07:41,840
so

155
00:07:38,940 --> 00:07:44,160
[Music]

156
00:07:41,840 --> 00:07:44,160
it's

157
00:07:49,780 --> 00:07:53,000
[Music]

158
00:07:56,319 --> 00:08:02,479
and it is still a fail so we want this

159
00:08:00,800 --> 00:08:06,000
to be equal

160
00:08:02,479 --> 00:08:09,199
uh to go across the next jump that it's

161
00:08:06,000 --> 00:08:11,280
printing this line here

162
00:08:09,199 --> 00:08:15,759
so this is the call probably that we

163
00:08:11,280 --> 00:08:15,759
want let's see what's printing there

164
00:08:24,960 --> 00:08:30,800
and the address

165
00:08:27,520 --> 00:08:30,800
or the print f

166
00:08:32,640 --> 00:08:36,000
g print

167
00:08:34,640 --> 00:08:39,000
b 9

168
00:08:36,000 --> 00:08:39,000
4.

169
00:08:44,240 --> 00:08:47,440
one eight four

170
00:08:50,399 --> 00:08:54,480
and that's the well done string so this

171
00:08:52,959 --> 00:08:55,279
is the right path

172
00:08:54,480 --> 00:08:57,440
well

173
00:08:55,279 --> 00:09:00,000
that we want to reach we want to be

174
00:08:57,440 --> 00:09:02,399
somewhere here and you can see that here

175
00:09:00,000 --> 00:09:05,200
it's loading the values that we

176
00:09:02,399 --> 00:09:05,920
stored from the scan f

177
00:09:05,200 --> 00:09:08,240
and

178
00:09:05,920 --> 00:09:10,399
using the printf function to print that

179
00:09:08,240 --> 00:09:12,240
string

180
00:09:10,399 --> 00:09:15,040
with the string format of g strings that

181
00:09:12,240 --> 00:09:16,880
we already saw without a white space in

182
00:09:15,040 --> 00:09:18,160
between

183
00:09:16,880 --> 00:09:19,040
so

184
00:09:18,160 --> 00:09:22,600
let's

185
00:09:19,040 --> 00:09:22,600
get into anger

186
00:09:22,800 --> 00:09:27,680
so first we need to set up the

187
00:09:24,880 --> 00:09:29,200
environment meaning we import angular

188
00:09:27,680 --> 00:09:30,000
clarify

189
00:09:29,200 --> 00:09:31,600
and

190
00:09:30,000 --> 00:09:34,480
cs for

191
00:09:31,600 --> 00:09:37,760
reasons it's just easier

192
00:09:34,480 --> 00:09:41,200
and then we can create our project with

193
00:09:37,760 --> 00:09:41,200
autoload libs false

194
00:09:43,200 --> 00:09:48,160
and now we need to define the initial

195
00:09:47,040 --> 00:09:51,600
state

196
00:09:48,160 --> 00:09:54,000
we can see here that the base address is

197
00:09:51,600 --> 00:09:56,399
loaded here so we can just give this

198
00:09:54,000 --> 00:09:59,600
address to the

199
00:09:56,399 --> 00:10:00,480
to angular to start from the beginning

200
00:09:59,600 --> 00:10:03,200
and

201
00:10:00,480 --> 00:10:04,880
our initial state is going to be

202
00:10:03,200 --> 00:10:08,000
project

203
00:10:04,880 --> 00:10:10,399
factory and we are going to tell

204
00:10:08,000 --> 00:10:14,399
anger to restart from the entry

205
00:10:10,399 --> 00:10:14,399
point of the binary interest state

206
00:10:16,880 --> 00:10:22,640
so what we are going to do is

207
00:10:19,519 --> 00:10:24,640
um basically run the binary onto the

208
00:10:22,640 --> 00:10:27,120
scanf

209
00:10:24,640 --> 00:10:30,560
and then inject our

210
00:10:27,120 --> 00:10:33,279
symbolic values in instead of

211
00:10:30,560 --> 00:10:36,240
having a flag

212
00:10:33,279 --> 00:10:38,160
and then we let anger reason about what

213
00:10:36,240 --> 00:10:40,959
possibilities we have

214
00:10:38,160 --> 00:10:44,399
for that string to end up in the

215
00:10:40,959 --> 00:10:47,920
well-done uh state that we know

216
00:10:44,399 --> 00:10:50,000
so this is the algorithm uh let's do it

217
00:10:47,920 --> 00:10:53,200
so first we need this kind of address

218
00:10:50,000 --> 00:10:54,079
because that's where we want to stop it

219
00:10:53,200 --> 00:10:56,240
uh

220
00:10:54,079 --> 00:10:58,560
stop anger

221
00:10:56,240 --> 00:11:01,560
so we go back

222
00:10:58,560 --> 00:11:01,560
and

223
00:11:02,320 --> 00:11:06,560
it was right in the beginning so here is

224
00:11:04,800 --> 00:11:10,000
this kind of call

225
00:11:06,560 --> 00:11:12,720
meaning we want to stop anger just right

226
00:11:10,000 --> 00:11:15,800
before it causes kind of so in there

227
00:11:12,720 --> 00:11:15,800
hex xh73

228
00:11:19,920 --> 00:11:23,440
here

229
00:11:21,839 --> 00:11:25,600
873

230
00:11:23,440 --> 00:11:27,680
and then our find address or the point

231
00:11:25,600 --> 00:11:30,640
that we are going to try to read is the

232
00:11:27,680 --> 00:11:32,240
base address and she with scanf

233
00:11:30,640 --> 00:11:34,800
we are going to

234
00:11:32,240 --> 00:11:36,800
start a simulation with the initial

235
00:11:34,800 --> 00:11:39,600
state that we just uh

236
00:11:36,800 --> 00:11:42,399
defined here that means from the entry

237
00:11:39,600 --> 00:11:43,600
point of the binary and explore until

238
00:11:42,399 --> 00:11:46,800
you are

239
00:11:43,600 --> 00:11:50,160
in a defined address we don't have any

240
00:11:46,800 --> 00:11:53,279
branching points leading us to triscan f

241
00:11:50,160 --> 00:11:54,720
it's a straight path so we should only

242
00:11:53,279 --> 00:11:55,839
have

243
00:11:54,720 --> 00:11:58,560
one

244
00:11:55,839 --> 00:12:00,959
found and it's uh very fast

245
00:11:58,560 --> 00:12:06,079
so what we are going to do is now store

246
00:12:00,959 --> 00:12:06,079
the state that the binary is right now

247
00:12:07,360 --> 00:12:13,120
and modify it in a way that we want it

248
00:12:10,720 --> 00:12:14,000
to have so what we are going to do as i

249
00:12:13,120 --> 00:12:15,360
say

250
00:12:14,000 --> 00:12:17,279
i said before

251
00:12:15,360 --> 00:12:20,000
is

252
00:12:17,279 --> 00:12:23,120
inject binary ah

253
00:12:20,000 --> 00:12:25,279
a symbolic uh bit vector instead of

254
00:12:23,120 --> 00:12:27,760
giving two concrete

255
00:12:25,279 --> 00:12:28,880
strings to the

256
00:12:27,760 --> 00:12:30,880
challenge

257
00:12:28,880 --> 00:12:35,200
so the way we can do that

258
00:12:30,880 --> 00:12:38,720
is creating a bit vector is a symbolic

259
00:12:35,200 --> 00:12:39,680
and we know that it needs to be to have

260
00:12:38,720 --> 00:12:42,800
like

261
00:12:39,680 --> 00:12:47,120
uh 11 hex or 17

262
00:12:42,800 --> 00:12:50,160
uh of length so we can pass the checks

263
00:12:47,120 --> 00:12:54,480
so this is how we do it like 80 bits per

264
00:12:50,160 --> 00:12:56,800
byte 17 characters 17 bytes

265
00:12:54,480 --> 00:13:00,079
so we declare these symbolic flags and

266
00:12:56,800 --> 00:13:02,399
then we store them

267
00:13:00,079 --> 00:13:03,839
at the same place where

268
00:13:02,399 --> 00:13:07,360
the

269
00:13:03,839 --> 00:13:10,480
binary is expecting to find our strings

270
00:13:07,360 --> 00:13:10,480
if we go back

271
00:13:11,040 --> 00:13:17,440
in the code

272
00:13:12,720 --> 00:13:17,440
we can see that after scanf

273
00:13:19,680 --> 00:13:24,480
like here on the scanf we are going to

274
00:13:22,560 --> 00:13:27,680
have our

275
00:13:24,480 --> 00:13:31,040
values first inside rsi

276
00:13:27,680 --> 00:13:34,079
and the second one in rdx

277
00:13:31,040 --> 00:13:38,240
so this is our these are the uh

278
00:13:34,079 --> 00:13:41,040
addresses that we need so let's

279
00:13:38,240 --> 00:13:43,519
store the addresses here

280
00:13:41,040 --> 00:13:45,680
so we need one in rsi

281
00:13:43,519 --> 00:13:49,839
and one in rdx

282
00:13:45,680 --> 00:13:49,839
then we inject or

283
00:13:50,399 --> 00:13:56,720
our symbolic uh

284
00:13:53,120 --> 00:14:00,079
flags that we declared here into that

285
00:13:56,720 --> 00:14:03,519
symbolic addresses that

286
00:14:00,079 --> 00:14:04,639
we just found from the binary

287
00:14:03,519 --> 00:14:07,839
so

288
00:14:04,639 --> 00:14:10,399
the way that we can do it in anger is uh

289
00:14:07,839 --> 00:14:13,279
using the state because we want to

290
00:14:10,399 --> 00:14:15,040
inject the values in this state that we

291
00:14:13,279 --> 00:14:17,600
we are right now

292
00:14:15,040 --> 00:14:20,000
and we want to inject it inject it into

293
00:14:17,600 --> 00:14:22,639
the memory so we use the memory and

294
00:14:20,000 --> 00:14:25,360
store and we are storing the symbolic

295
00:14:22,639 --> 00:14:27,120
values into that address in the memory

296
00:14:25,360 --> 00:14:30,240
that we just found

297
00:14:27,120 --> 00:14:32,560
now it's the time that we need to help

298
00:14:30,240 --> 00:14:35,920
anger a little bit

299
00:14:32,560 --> 00:14:39,279
as we know that it is uh

300
00:14:35,920 --> 00:14:41,519
a ctf and it's expecting strings we can

301
00:14:39,279 --> 00:14:43,440
constrain the range of the input of

302
00:14:41,519 --> 00:14:45,600
these symbolic values

303
00:14:43,440 --> 00:14:48,320
into the printable charts because we

304
00:14:45,600 --> 00:14:50,800
know that it's going to be

305
00:14:48,320 --> 00:14:54,240
characters

306
00:14:50,800 --> 00:14:57,920
the second thing that we can

307
00:14:54,240 --> 00:15:01,519
constrain is the beginning of the flag

308
00:14:57,920 --> 00:15:04,800
that it's uh basically shakti ctf every

309
00:15:01,519 --> 00:15:06,639
cjf has this flag format and that's what

310
00:15:04,800 --> 00:15:09,040
we are going to add

311
00:15:06,639 --> 00:15:11,120
what we do is always support the

312
00:15:09,040 --> 00:15:13,360
symbolic execution with all the

313
00:15:11,120 --> 00:15:16,480
constraints or information that we have

314
00:15:13,360 --> 00:15:18,880
already so that it's faster and doesn't

315
00:15:16,480 --> 00:15:21,440
need to reason about things that we

316
00:15:18,880 --> 00:15:24,000
already know

317
00:15:21,440 --> 00:15:26,880
so the next one is a little bit of a

318
00:15:24,000 --> 00:15:28,880
trick because it's not really how you

319
00:15:26,880 --> 00:15:29,759
normally do things

320
00:15:28,880 --> 00:15:32,240
uh

321
00:15:29,759 --> 00:15:33,279
it's more like a way that it works all

322
00:15:32,240 --> 00:15:37,440
the time

323
00:15:33,279 --> 00:15:39,519
if as we are still not using the hooking

324
00:15:37,440 --> 00:15:40,399
for this kind of function what we want

325
00:15:39,519 --> 00:15:43,120
to do

326
00:15:40,399 --> 00:15:44,480
is basically skip that call

327
00:15:43,120 --> 00:15:46,720
because

328
00:15:44,480 --> 00:15:49,600
if we call scanf

329
00:15:46,720 --> 00:15:52,000
right now it's going to mess up with the

330
00:15:49,600 --> 00:15:54,959
state that we just created

331
00:15:52,000 --> 00:15:58,560
so what we want to do is basically skip

332
00:15:54,959 --> 00:16:00,079
this call right now we are

333
00:15:58,560 --> 00:16:01,680
here

334
00:16:00,079 --> 00:16:03,360
in the

335
00:16:01,680 --> 00:16:07,199
873.

336
00:16:03,360 --> 00:16:09,680
so if we skip the call

337
00:16:07,199 --> 00:16:11,279
to scanf and just go

338
00:16:09,680 --> 00:16:12,959
to the next

339
00:16:11,279 --> 00:16:14,320
um

340
00:16:12,959 --> 00:16:16,480
instruction

341
00:16:14,320 --> 00:16:18,240
basically not paying this one

342
00:16:16,480 --> 00:16:20,959
uh we can see that the difference

343
00:16:18,240 --> 00:16:23,199
between these addresses is like a

344
00:16:20,959 --> 00:16:25,040
uh if it's easier for you or you can see

345
00:16:23,199 --> 00:16:26,399
here it's plus

346
00:16:25,040 --> 00:16:30,399
57

347
00:16:26,399 --> 00:16:32,480
here is plus 67 so we are skipping 10

348
00:16:30,399 --> 00:16:33,519
uh bytes and that's what we are going to

349
00:16:32,480 --> 00:16:36,759
do

350
00:16:33,519 --> 00:16:36,759
in anger

351
00:16:37,519 --> 00:16:41,040
so you don't really need to understand

352
00:16:40,000 --> 00:16:43,120
how

353
00:16:41,040 --> 00:16:45,839
um

354
00:16:43,120 --> 00:16:48,240
how the whole architecture works you can

355
00:16:45,839 --> 00:16:50,959
just like easy logic and

356
00:16:48,240 --> 00:16:52,399
move the instruction pointer

357
00:16:50,959 --> 00:16:55,120
10

358
00:16:52,399 --> 00:16:55,120
likes already

359
00:16:55,199 --> 00:17:01,360
so what we do now

360
00:16:58,079 --> 00:17:04,319
is basically start the new simulation

361
00:17:01,360 --> 00:17:06,559
from that point

362
00:17:04,319 --> 00:17:06,559
so

363
00:17:07,039 --> 00:17:12,880
we start the simulation again

364
00:17:09,600 --> 00:17:15,600
but this time we put the name on we

365
00:17:12,880 --> 00:17:18,480
start with the state that we modified

366
00:17:15,600 --> 00:17:21,360
with all these constraints and

367
00:17:18,480 --> 00:17:24,160
and or symbolic uh

368
00:17:21,360 --> 00:17:27,760
or symbolic play what we are going to do

369
00:17:24,160 --> 00:17:30,559
here is basically explore until we find

370
00:17:27,760 --> 00:17:34,320
the success this thing this we can do

371
00:17:30,559 --> 00:17:37,200
exactly the same way we did in order

372
00:17:34,320 --> 00:17:39,200
examples in china so we create like a

373
00:17:37,200 --> 00:17:41,280
small lambda function that it's looking

374
00:17:39,200 --> 00:17:43,919
for a string

375
00:17:41,280 --> 00:17:46,240
and we know that uh our string here is

376
00:17:43,919 --> 00:17:48,160
your flag or something so we just type

377
00:17:46,240 --> 00:17:51,040
that

378
00:17:48,160 --> 00:17:52,960
and we are going to look for it into the

379
00:17:51,040 --> 00:17:54,799
[Music]

380
00:17:52,960 --> 00:17:58,559
instincts

381
00:17:54,799 --> 00:17:58,559
uh it needs to be in the

382
00:17:58,640 --> 00:18:01,039
s

383
00:17:59,280 --> 00:18:04,080
is standard out

384
00:18:01,039 --> 00:18:07,280
so let's see the thumbs

385
00:18:04,080 --> 00:18:07,280
and we let it run

386
00:18:13,600 --> 00:18:17,760
there we go we found one so let's check

387
00:18:16,160 --> 00:18:20,240
what we found

388
00:18:17,760 --> 00:18:24,080
for that we can you you can

389
00:18:20,240 --> 00:18:26,080
say that we we have the neo found

390
00:18:24,080 --> 00:18:30,480
and these uh

391
00:18:26,080 --> 00:18:33,360
is going to be evaluated or concretised

392
00:18:30,480 --> 00:18:34,480
and casted to bite so we can read what's

393
00:18:33,360 --> 00:18:37,440
actually

394
00:18:34,480 --> 00:18:38,400
being printed and here you are here we

395
00:18:37,440 --> 00:18:40,000
can see

396
00:18:38,400 --> 00:18:43,760
it's hello there

397
00:18:40,000 --> 00:18:45,760
uh the well done um string and then we

398
00:18:43,760 --> 00:18:48,240
have here's your flag

399
00:18:45,760 --> 00:18:50,960
but as you can see normally the flag

400
00:18:48,240 --> 00:18:53,440
format has like uh

401
00:18:50,960 --> 00:18:57,600
that's not it's not like a name here you

402
00:18:53,440 --> 00:19:01,039
need to close that painted this thing

403
00:18:57,600 --> 00:19:03,919
and also we can see that this is not

404
00:19:01,039 --> 00:19:07,280
really making any sense

405
00:19:03,919 --> 00:19:10,000
as well as the tick here so

406
00:19:07,280 --> 00:19:12,320
our string is not perfect we can try to

407
00:19:10,000 --> 00:19:14,400
put it as an output there

408
00:19:12,320 --> 00:19:15,919
but we we can already see that it's not

409
00:19:14,400 --> 00:19:18,799
really right

410
00:19:15,919 --> 00:19:21,760
so now we use this uh here

411
00:19:18,799 --> 00:19:24,960
so this seems wrong what we can do

412
00:19:21,760 --> 00:19:28,320
uh is simply telling tell the silver

413
00:19:24,960 --> 00:19:31,360
that which bite is wrong

414
00:19:28,320 --> 00:19:34,080
and say like please fix it and what we

415
00:19:31,360 --> 00:19:35,440
do is like we can uh have the tick for

416
00:19:34,080 --> 00:19:38,000
example here

417
00:19:35,440 --> 00:19:41,440
we can also add another

418
00:19:38,000 --> 00:19:42,480
constraint that we know that it's

419
00:19:41,440 --> 00:19:43,440
wrong

420
00:19:42,480 --> 00:19:45,840
that it's

421
00:19:43,440 --> 00:19:47,280
the type for example that it's not a

422
00:19:45,840 --> 00:19:48,400
number or

423
00:19:47,280 --> 00:19:51,440
a letter

424
00:19:48,400 --> 00:19:53,679
so we can use

425
00:19:51,440 --> 00:19:55,440
that also as a constraint so that's in

426
00:19:53,679 --> 00:19:57,919
the byte 5 of

427
00:19:55,440 --> 00:19:58,799
the second part of the flag

428
00:19:57,919 --> 00:20:02,640
and

429
00:19:58,799 --> 00:20:02,640
what we know it it isn't

430
00:20:04,159 --> 00:20:11,919
uh it isn't a pipe

431
00:20:07,679 --> 00:20:11,919
what we can also do is uh

432
00:20:12,159 --> 00:20:16,960
let's see if it works like this

433
00:20:14,240 --> 00:20:20,320
first so to add these constraints what

434
00:20:16,960 --> 00:20:22,480
we need to do is add the constraint

435
00:20:20,320 --> 00:20:25,440
but not in this stage that we are right

436
00:20:22,480 --> 00:20:28,960
now because we modified it right so what

437
00:20:25,440 --> 00:20:29,330
we need to do is go back

438
00:20:28,960 --> 00:20:32,080
to

439
00:20:29,330 --> 00:20:34,400
[Music]

440
00:20:32,080 --> 00:20:37,520
to this uh to the beginning

441
00:20:34,400 --> 00:20:38,799
and just run it again so we find again

442
00:20:37,520 --> 00:20:42,400
the clean

443
00:20:38,799 --> 00:20:42,400
state that we had before

444
00:20:42,559 --> 00:20:48,480
we store it again in the state we

445
00:20:44,480 --> 00:20:50,799
generate new uh symbolic values we store

446
00:20:48,480 --> 00:20:52,960
the addresses that are the same

447
00:20:50,799 --> 00:20:55,039
we store them in the memory in that

448
00:20:52,960 --> 00:20:57,200
addresses that we had

449
00:20:55,039 --> 00:21:00,080
and then we had all the constraints that

450
00:20:57,200 --> 00:21:02,640
we have now

451
00:21:00,080 --> 00:21:04,159
now we can skip the

452
00:21:02,640 --> 00:21:06,880
the invites again

453
00:21:04,159 --> 00:21:08,559
uh it doesn't matter in which sequence

454
00:21:06,880 --> 00:21:13,200
we make this

455
00:21:08,559 --> 00:21:13,200
and then we run the simulation again

456
00:21:13,919 --> 00:21:17,760
we found another one so that's good

457
00:21:16,480 --> 00:21:19,840
let's

458
00:21:17,760 --> 00:21:21,600
print

459
00:21:19,840 --> 00:21:23,520
as you can see now we have something

460
00:21:21,600 --> 00:21:26,960
that makes sense it's even saying anger

461
00:21:23,520 --> 00:21:28,840
here but we still have this m instead of

462
00:21:26,960 --> 00:21:32,000
the closing

463
00:21:28,840 --> 00:21:34,000
parenthesis and we have a tudor in here

464
00:21:32,000 --> 00:21:36,240
so let's constrain that a little bit

465
00:21:34,000 --> 00:21:36,240
more

466
00:21:38,559 --> 00:21:45,640
so what we know now is that the flag

467
00:21:42,640 --> 00:21:45,640
to

468
00:21:45,919 --> 00:21:51,760
on the byte uh

469
00:21:48,799 --> 00:21:54,240
five that it was the pipeline before

470
00:21:51,760 --> 00:21:55,919
is a isn't a tudor

471
00:21:54,240 --> 00:21:59,120
so let's

472
00:21:55,919 --> 00:22:02,000
remove that one

473
00:21:59,120 --> 00:22:04,080
we could try to add another

474
00:22:02,000 --> 00:22:05,120
uh constraint but let's just

475
00:22:04,080 --> 00:22:08,000
try to

476
00:22:05,120 --> 00:22:10,400
keep it at a minimum let's see if anger

477
00:22:08,000 --> 00:22:12,640
can do it for us

478
00:22:10,400 --> 00:22:16,159
so again we

479
00:22:12,640 --> 00:22:18,559
restart get the clean uh

480
00:22:16,159 --> 00:22:21,360
state before the scanf

481
00:22:18,559 --> 00:22:26,919
we store it we modify with our symbolic

482
00:22:21,360 --> 00:22:26,919
values we add our constraints again

483
00:22:27,760 --> 00:22:31,600
and then we

484
00:22:30,400 --> 00:22:33,840
skip

485
00:22:31,600 --> 00:22:33,840
the

486
00:22:33,919 --> 00:22:37,039
10 bytes and

487
00:22:35,760 --> 00:22:40,760
run the

488
00:22:37,039 --> 00:22:40,760
simulation again

489
00:22:44,799 --> 00:22:50,400
there we are we found another one

490
00:22:46,880 --> 00:22:52,720
let's see what we found this time

491
00:22:50,400 --> 00:22:56,480
and look

492
00:22:52,720 --> 00:22:59,520
z3 is the other way of solving this

493
00:22:56,480 --> 00:23:01,039
challenge the link to see how the team

494
00:22:59,520 --> 00:23:04,000
shakti

495
00:23:01,039 --> 00:23:05,679
so with it using z3 pi

496
00:23:04,000 --> 00:23:07,200
is

497
00:23:05,679 --> 00:23:09,120
here on

498
00:23:07,200 --> 00:23:11,520
on the course you can click and have a

499
00:23:09,120 --> 00:23:16,720
look at the script there

500
00:23:11,520 --> 00:23:21,799
and as we fix the z here we also fix

501
00:23:16,720 --> 00:23:21,799
the closing of the flag format

502
00:23:31,200 --> 00:23:36,880
so what we can do is now print

503
00:23:34,000 --> 00:23:39,200
uh i just printed with like uh

504
00:23:36,880 --> 00:23:42,640
a small white space in between this so

505
00:23:39,200 --> 00:23:44,559
that we know uh the 17 and the 17 in a

506
00:23:42,640 --> 00:23:46,640
way that we don't need to

507
00:23:44,559 --> 00:23:48,880
like uh

508
00:23:46,640 --> 00:23:52,799
suffer too much

509
00:23:48,880 --> 00:23:54,720
so what we are trying going to do is

510
00:23:52,799 --> 00:23:55,600
use the

511
00:23:54,720 --> 00:23:59,120
this

512
00:23:55,600 --> 00:24:01,120
flag now and try to solve the challenge

513
00:23:59,120 --> 00:24:03,679
so we can

514
00:24:01,120 --> 00:24:07,279
put our first half of the flag

515
00:24:03,679 --> 00:24:07,279
and now we go back

516
00:24:07,840 --> 00:24:11,279
get the second part

517
00:24:12,400 --> 00:24:16,640
and there you go

518
00:24:14,080 --> 00:24:18,400
which is of the moon challenge

519
00:24:16,640 --> 00:24:23,200
again have a look

520
00:24:18,400 --> 00:24:23,200
at the z3 solution from the team shakti

