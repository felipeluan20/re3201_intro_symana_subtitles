1
00:00:00,310 --> 00:00:06,918
So, let's solve the same
problem we solved with the SMT

2
00:00:06,943 --> 00:00:12,932
but with Python and z3 Python
bindings that I was talking about.

3
00:00:12,945 --> 00:00:21,884
So, the Python bindings are documented
in Python web pages of it.

4
00:00:21,896 --> 00:00:26,490
So, as you can see here,
there is the z3prover.github.io,

5
00:00:26,490 --> 00:00:32,753
documenting the whole API and everything
that is available under the z3py.

6
00:00:32,778 --> 00:00:36,866
You have all the data structures
and functions, the variables,

7
00:00:36,891 --> 00:00:40,447
and in the end, you also have
the function documentation

8
00:00:40,472 --> 00:00:43,248
where you can see use-cases
and how it looks like,

9
00:00:43,273 --> 00:00:46,209
and the implementation
of the function itself.

10
00:00:46,210 --> 00:00:48,887
what is pretty useful sometimes

11
00:00:48,899 --> 00:00:53,220
if you don't know
what you're doing,

12
00:00:53,245 --> 00:00:58,949
and you have a link here
directly from your VM

13
00:00:58,973 --> 00:01:06,310
And we can install the python bindings
for z3 with pip install z3-solver.

14
00:01:06,335 --> 00:01:09,117
I always recommend having
a virtual environment

15
00:01:09,142 --> 00:01:13,368
or something separated
for z3 and for SMT.

16
00:01:13,380 --> 00:01:18,294
This VM is just for it,
so, I'm not having it here,

17
00:01:18,319 --> 00:01:21,256
but it's something to keep in mind.

18
00:01:21,281 --> 00:01:27,905
So, with control-enter, you can run
anything that is inside the cell.

19
00:01:27,930 --> 00:01:34,042
So, try it and see if you can install
this z3 solver into your machine.

20
00:01:34,067 --> 00:01:36,780
If everything goes well,

21
00:01:36,805 --> 00:01:44,918
we can see the snippet we already
solved using SMT language.

22
00:01:44,930 --> 00:01:51,607
And again, I have a small skeleton here,
but we are going to do it together.

23
00:01:51,632 --> 00:01:55,674
As always, if you prefer
doing it by yourself,

24
00:01:55,699 --> 00:02:00,777
please, do so and just press
pause right now and come back

25
00:02:00,789 --> 00:02:05,475
if you want to review or see
another way of doing it.

26
00:02:05,500 --> 00:02:10,255
You can also just join us
back again or keep listening.

27
00:02:10,280 --> 00:02:14,253
and we are going to walk
through this challenge.

28
00:02:14,278 --> 00:02:19,019
So, first of all, we need
to set up the environment,

29
00:02:19,020 --> 00:02:21,672
and importantly the python bindings.

30
00:02:21,697 --> 00:02:25,879
The best way of doing it
is from z3 import.

31
00:02:25,879 --> 00:02:31,362
And every time we do this, we need to
remember that we need to create a solver,

32
00:02:31,387 --> 00:02:34,208
because Python is not the solver itself.

33
00:02:34,220 --> 00:02:37,060
It's just binding, so we always
need to create a solver.

34
00:02:37,060 --> 00:02:39,587
So, let's do that.

35
00:02:39,612 --> 00:02:48,917
So again, as before, we need to declare the
variables that we have in our assembly code.

36
00:02:48,942 --> 00:02:53,476
So here, we can declare
everything we want.

37
00:02:53,501 --> 00:02:58,890
In the case of the eax input, we
need to use like the z3 bit vector.

38
00:02:58,915 --> 00:03:03,668
We'll give a name for it,
like we did before in SMT.

39
00:03:03,693 --> 00:03:07,850
And we need this size,
exactly like before.

40
00:03:07,862 --> 00:03:12,549
So, doing it for ecx_in should be trivial.

41
00:03:12,549 --> 00:03:14,239
So, z3.BitVec,

42
00:03:14,264 --> 00:03:17,279
and then, we need to
give a name for it.

43
00:03:17,304 --> 00:03:22,295
And let's use ecx_in again.
so, it's consistent.

44
00:03:22,320 --> 00:03:26,304
And then, we add the size, 32.

45
00:03:26,329 --> 00:03:32,653
For...the good part of the python is
that you can also create lists

46
00:03:32,665 --> 00:03:38,163
and create multiple variables
in one line of code.

47
00:03:38,188 --> 00:03:43,425
I like to create a per
register this way.

48
00:03:43,450 --> 00:03:47,780
So, I know how many states
I have for each register,

49
00:03:47,805 --> 00:03:50,666
and I can just keep
adding it together.

50
00:03:50,692 --> 00:03:53,088
So here, I have this for eax

51
00:03:53,113 --> 00:03:56,067
As we know from the SMT solution,

52
00:03:56,092 --> 00:03:59,260
we need like another two edx registers.

53
00:03:59,261 --> 00:04:02,246
So, that's what we're going to do.

54
00:04:02,271 --> 00:04:06,191
And the difference
here is just the 's.'

55
00:04:06,216 --> 00:04:09,378
So, we are not creating one bit vector.

56
00:04:09,403 --> 00:04:13,106
We are creating more bit vectors,
and that's all.

57
00:04:13,131 --> 00:04:20,708
And we can create edx1 and edx2,

58
00:04:20,720 --> 00:04:25,722
and both of them are
going to be 32 bit.

59
00:04:25,747 --> 00:04:29,243
So, Control-Enter and that's it.

60
00:04:29,268 --> 00:04:31,284
So, we keep scrolling.

61
00:04:31,309 --> 00:04:36,178
And now, what we need to
do is model the problem.

62
00:04:36,203 --> 00:04:40,953
As you can remember from the
past video and exercise,

63
00:04:40,978 --> 00:04:48,839
what we did was to create somehow an AND
connection between different constraints,

64
00:04:48,864 --> 00:04:51,870
and that's basically the model.

65
00:04:51,870 --> 00:04:55,068
We are creating here,
the same five lines of code,

66
00:04:55,093 --> 00:04:59,674
and with the double equal,
we are asserting that it's equal.

67
00:04:59,699 --> 00:05:03,529
So, that's also a way to say
that it's an assertion,

68
00:05:03,554 --> 00:05:08,606
and we are connecting it with a comma,
which means that they are AND connected.

69
00:05:08,631 --> 00:05:11,547
All of them need to be the same.

70
00:05:11,572 --> 00:05:15,609
So, the first one was an 'add,'

71
00:05:15,634 --> 00:05:18,901
So, we can actually use an add symbol.

72
00:05:18,926 --> 00:05:20,781
With the shift, it's the same.

73
00:05:20,806 --> 00:05:24,016
We can also use the shift left.

74
00:05:24,041 --> 00:05:28,644
And for this, we need to have a
BitVecVal from ecx if you can remember

75
00:05:30,368 --> 00:05:35,366
You can also create it in Python
just using the value appended here

76
00:05:35,392 --> 00:05:39,221
And then, you have the
value and the size again,

77
00:05:39,246 --> 00:05:47,269
creating a Bitvector of value 12
of 32 bit size.

78
00:05:47,270 --> 00:05:50,616
And then, the third line
was the multiplication.

79
00:05:50,641 --> 00:05:52,203
As we know assembly

80
00:05:52,228 --> 00:05:55,386
We know that we need to take the eax2

81
00:05:55,411 --> 00:05:59,542
That was the value of a eax
right before this instruction.

82
00:05:59,554 --> 00:06:05,439
And we have multiplied by ecx_in.

83
00:06:05,464 --> 00:06:09,440
And then, we connect
it with the a comma

84
00:06:09,464 --> 00:06:11,364
And then, we have edx1.

85
00:06:11,389 --> 00:06:13,622
edx1 was in the mov

86
00:06:15,075 --> 00:06:20,742
So, we want to assert that it's
equal to eax right before eax3.

87
00:06:20,767 --> 00:06:23,927
And then the last one was the XOR.

88
00:06:23,952 --> 00:06:26,282
So, we can use the XOR symbol.

89
00:06:26,307 --> 00:06:32,596
And then, we have the edx1 XORed with ecx_in.

90
00:06:32,621 --> 00:06:36,115
And that's how we model the problem.

91
00:06:36,141 --> 00:06:38,205
It's very simple.

92
00:06:38,230 --> 00:06:42,453
It's just a list of assertions in python.

93
00:06:42,465 --> 00:06:45,089
Then we get to the next one,

94
00:06:45,114 --> 00:06:48,347
and to add this model to the solver,

95
00:06:48,372 --> 00:06:54,811
because right now, you'll just define it,
but we still need to add it to our solver.

96
00:06:54,836 --> 00:06:57,443
So, we can just do s.add(model),

97
00:06:57,455 --> 00:07:03,330
and it's going to add all the
constraints that we have in this list.

98
00:07:03,355 --> 00:07:06,281
We could also have a solver add,

99
00:07:06,306 --> 00:07:11,264
and then, each of these single
lines being added to the solver.

100
00:07:11,289 --> 00:07:15,406
That would also work, but
I like to keep it clean.

101
00:07:15,431 --> 00:07:18,994
And that's why I define
the problem like this,

102
00:07:19,019 --> 00:07:23,251
and then, I add constraints,
separated when it makes sense,

103
00:07:23,276 --> 00:07:26,049
when I'm really constraining my output.

104
00:07:26,061 --> 00:07:33,548
As before, the first thing that we need
to do is to check if there is a solution.

105
00:07:33,573 --> 00:07:38,944
So, there is a function that is the
check-SAT in Python, that is the check()

106
00:07:39,100 --> 00:07:43,613
So, we can call it from the solver

107
00:07:45,020 --> 00:07:53,464
And the answer or the return is going
to be z3 sat or as z3 unsat.

108
00:07:53,489 --> 00:07:58,357
The only thing that I do here
is if you cannot find a solution just quit,

109
00:07:58,382 --> 00:08:01,925
because I like to know
that it has a solution

110
00:08:01,950 --> 00:08:05,468
before running any kind
of more complex code.

111
00:08:05,493 --> 00:08:11,316
So, it didn't quit and didn't print,
so, we are fine.

112
00:08:11,341 --> 00:08:13,625
We probably have a solution.

113
00:08:13,650 --> 00:08:18,462
So, what we are going to do now is,

114
00:08:18,487 --> 00:08:22,812
we can also like print or
check just to be sure,

115
00:08:22,837 --> 00:08:25,107
like just adding it here

116
00:08:28,620 --> 00:08:31,278
like this.

117
00:08:31,303 --> 00:08:36,636
And you can see that our
s.check() is returning sat.

118
00:08:36,661 --> 00:08:41,642
So, let's keep adding the
constraints like before.

119
00:08:41,667 --> 00:08:46,321
So I like to give a name
for each constraint,

120
00:08:46,346 --> 00:08:52,316
just because as I said you could just
like copy-paste this part here.

121
00:08:52,341 --> 00:08:56,391
So, my next requirement or
constraint is like the edx2.

122
00:08:56,416 --> 00:09:01,908
Now, it's like a bit
vector of value 100h,

123
00:09:01,933 --> 00:09:05,393
So, I'm adding that
requirement to my solver.

124
00:09:05,418 --> 00:09:08,975
But you can add anything you want.

125
00:09:09,000 --> 00:09:13,040
And then, I again check for a solution.

126
00:09:13,040 --> 00:09:16,873
And if I have a solution this time,

127
00:09:16,898 --> 00:09:20,132
I'm going to have three values,

128
00:09:20,157 --> 00:09:23,097
the 'm,' the eax value,
and the ecx value,

129
00:09:23,122 --> 00:09:25,763
because now I'm leaving the end value

130
00:09:25,775 --> 00:09:31,039
and I want to know the input that
would get me to this output.

131
00:09:31,064 --> 00:09:37,174
That's something that we do a lot while fuzzing
and when you actually find a bug,

132
00:09:37,199 --> 00:09:42,140
and you need to know which kind of
input is going to trigger that bug.

133
00:09:42,165 --> 00:09:44,554
That's the kind of thing that we do.

134
00:09:44,579 --> 00:09:46,667
We set the output for the bug,

135
00:09:46,692 --> 00:09:51,728
and we try to find out which
conditions do we have for the input

136
00:09:51,753 --> 00:09:54,275
to actually trigger that path,

137
00:09:54,300 --> 00:09:56,990
and that's what we are going to do.

138
00:09:57,015 --> 00:10:00,973
So, to find that,
we have here the s.check().

139
00:10:00,998 --> 00:10:07,633
Again, this time, we want to be
sure that we get a solution.

140
00:10:07,658 --> 00:10:12,102
And to get the model,
we have the model.

141
00:10:12,127 --> 00:10:20,304
That's the same as the get model
that we had before in the SMT path

142
00:10:20,329 --> 00:10:22,365
or in the SMT solution.

143
00:10:22,390 --> 00:10:26,820
And to get the eax value from this,

144
00:10:26,820 --> 00:10:29,409
we need to call it from the model.

145
00:10:29,434 --> 00:10:32,716
So, we have a model,
and in this model,

146
00:10:32,741 --> 00:10:40,193
we know that our input for
the eax was called eax_in,

147
00:10:40,218 --> 00:10:43,236
because that was the
name that we gave to it.

148
00:10:43,261 --> 00:10:46,937
And as we know, it's
going to be in bytes.

149
00:10:46,962 --> 00:10:49,145
So, we can just print it as long.

150
00:10:49,170 --> 00:10:53,734
And the same way you
can do for the ecx_in,

151
00:10:53,759 --> 00:10:58,755
and you also want to
know the input value.

152
00:10:58,780 --> 00:11:00,730
This looks nice.

153
00:11:02,188 --> 00:11:05,391
And then, I just have a pretty-print here.

154
00:11:05,416 --> 00:11:09,816
So, let's see if we have a solution.

155
00:11:09,841 --> 00:11:16,728
And as you can see, we found a
solution where the edx2 value

156
00:11:16,753 --> 00:11:25,616
That means the end of our five instructions
is going to be a 100h as a bit vector.

157
00:11:25,641 --> 00:11:28,014
And the solution would be,

158
00:11:28,039 --> 00:11:37,320
if we started eax like 0xfff00
and with ecx like 100h,

159
00:11:38,310 --> 00:11:41,186
If we have some kind of requirement

160
00:11:41,211 --> 00:11:48,151
that it's like something that
turns the solution unfeasible,

161
00:11:48,176 --> 00:11:51,163
then, it just wouldn't print,
cannot solve it.

162
00:11:51,188 --> 00:11:56,451
So, what's interesting about
using Python instead of z3

163
00:11:56,476 --> 00:12:00,095
since we could do that in z3 already,

164
00:12:00,120 --> 00:12:02,157
so, it's nothing new.

165
00:12:02,181 --> 00:12:04,797
It's the part of the iteration.

166
00:12:04,822 --> 00:12:10,730
As I was talking with you all before,
once you find one kind of solution,

167
00:12:10,731 --> 00:12:14,061
but it's not the solution
that you want to have,

168
00:12:14,086 --> 00:12:18,989
sometimes you just want to
automate that kind of restriction,

169
00:12:19,014 --> 00:12:22,934
and that's what you can do
very easily with Python.

170
00:12:22,959 --> 00:12:28,735
And that's why the Python bindings or
the C bindings are the preferred way

171
00:12:28,760 --> 00:12:31,897
because you can automate this kind of things

172
00:12:31,922 --> 00:12:33,839
So. we can do it together.

173
00:12:33,850 --> 00:12:39,327
I'll show you how I normally do
this kind of simple automation,

174
00:12:39,352 --> 00:12:41,637
but in a very verbose way.

175
00:12:41,649 --> 00:12:46,635
And then, you can just get the
idea and try it yourself.

176
00:12:46,660 --> 00:12:49,959
So normally, this kind of iterations,

177
00:12:49,971 --> 00:12:53,135
you just have like some kind of while(True),

178
00:12:53,160 --> 00:13:00,830
In our case, we are just interested as long
as we have some kind of feasible solution.

179
00:13:00,831 --> 00:13:05,706
So, that's my condition for my while()

180
00:13:05,717 --> 00:13:10,400
Every time when I have a new solution,

181
00:13:10,400 --> 00:13:11,824
it means I have a model.

182
00:13:11,849 --> 00:13:14,992
So, I need to retrieve that
model every single time.

183
00:13:15,017 --> 00:13:19,757
So, I can retrieve all the
information from my execution.

184
00:13:19,782 --> 00:13:24,274
And again, my eax_val,

185
00:13:24,299 --> 00:13:28,064
I already know how
to retrieve that.

186
00:13:28,089 --> 00:13:46,740
I will use that as just like an int here because
I am abusing the python f-strings

187
00:13:46,741 --> 00:13:50,502
So, we are going to get
the input and the output,

188
00:13:50,513 --> 00:13:52,603
from the eax and the ecx...

189
00:13:54,681 --> 00:13:58,535
As you already noticed,
I am the laziest person,

190
00:13:58,560 --> 00:13:59,766
so, I'm just copy-pasting,

191
00:13:59,791 --> 00:14:05,045
but basically, what we are doing,
we are storing the value that we found

192
00:14:05,070 --> 00:14:10,333
just like before for eax input and ecx
input that gets us to that solution.

193
00:14:10,358 --> 00:14:17,161
And this here, we are going to use this
information to update our requirements.

194
00:14:17,186 --> 00:14:23,946
So, we are going to create
our requirement for eax.

195
00:14:23,971 --> 00:14:36,519
Basically, eax_in should not
be the one that we just got,

196
00:14:36,544 --> 00:14:39,253
because that one we already know

197
00:14:39,279 --> 00:14:41,061
Basically this.

198
00:14:41,086 --> 00:14:45,484
And the same way we are
going to do for ecx.

199
00:14:45,509 --> 00:14:48,872
So, we are going to have a
new requirement for ecx.

200
00:14:48,897 --> 00:14:53,516
This means that ecx_in is not
the same value that we just got,

201
00:14:53,541 --> 00:14:55,974
because we already know.

202
00:14:55,999 --> 00:15:06,067
And we are going to add this
requirement to our solver,

203
00:15:06,092 --> 00:15:12,107
and the same way we'll do for the ecx.

204
00:15:12,132 --> 00:15:16,330
So, we have both of them.

205
00:15:16,356 --> 00:15:25,774
And now, what we want to do is also keep
a track of all the possible solutions

206
00:15:25,799 --> 00:15:27,697
that you are going to find.

207
00:15:27,722 --> 00:15:33,301
So, that's why I created this list with the
eax_in and ecx_in that we found before.

208
00:15:33,326 --> 00:15:40,201
So, that's the first one,
and I'm going to append all my list,

209
00:15:40,226 --> 00:15:58,077
the new ones that I just found right now,
and that is eax_in and and ecx_in

210
00:16:04,040 --> 00:16:05,435
and because I like to see stuff,

211
00:16:05,460 --> 00:16:09,802
I can just print something
similar like before,

212
00:16:09,827 --> 00:16:24,547
create an f-string and I have that my eax is going
to be something like the eax_in

213
00:16:24,572 --> 00:16:26,611
that we found before,

214
00:16:26,636 --> 00:16:30,026
and I can format it 02x...

215
00:16:30,051 --> 00:16:41,780
And then, we have the ECX,
and just like the same.

216
00:16:44,848 --> 00:16:48,661
What I'm going to do is
just break after ten,

217
00:16:48,674 --> 00:16:51,529
because we have a lot of
possible solutions on this case.

218
00:16:51,529 --> 00:16:56,626
And what's happening now is like running it,

219
00:16:56,651 --> 00:17:01,439
we'll get different possibilities.

220
00:17:01,464 --> 00:17:08,933
And what's happening is like I'm adding tuples
like if ecx keeps being 100h,

221
00:17:08,958 --> 00:17:12,934
it's going to be this, but at some point,
we don't have any more hundreds,

222
00:17:12,959 --> 00:17:15,287
and then, it's going
to find other ecxs.

223
00:17:15,312 --> 00:17:19,502
So, I'm going to get all the
possible pairs of input

224
00:17:19,527 --> 00:17:22,506
that satisfy the output that I chose.

225
00:17:22,532 --> 00:17:26,216
And this way, I know
which kind of inputs

226
00:17:26,241 --> 00:17:30,274
for my program would trigger
my bug for example.

