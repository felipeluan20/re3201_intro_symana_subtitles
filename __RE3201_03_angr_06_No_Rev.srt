﻿1
00:00:00,160 --> 00:00:04,418
So, the next exercise is
not even an exercise.

2
00:00:04,443 --> 00:00:08,941
We are just going to see
ihow we can solve CTF challenges

3
00:00:08,966 --> 00:00:14,627
that are classified as reversing
without any reversing at all.

4
00:00:14,652 --> 00:00:21,908
I hope you tried to do it yourself
before looking through this walkthrough.

5
00:00:21,933 --> 00:00:25,491
The solution is here.

6
00:00:26,156 --> 00:00:28,928
We have the skeleton.

7
00:00:30,800 --> 00:00:35,911
You can have your reverse engineer
workflow here like I had before,

8
00:00:36,411 --> 00:00:42,623
like I would use the file,
and then, the path to my binary.

9
00:00:42,648 --> 00:00:45,784
And this one is in the A.

10
00:00:47,600 --> 00:00:53,360
The name of our binary is
Tokyowesterns and a hash.

11
00:00:56,903 --> 00:01:03,186
This binary is an ELF, 32-bit.

12
00:01:03,211 --> 00:01:07,981
So, let's try to solve this challenge
without even looking into the binary.

13
00:01:08,006 --> 00:01:15,544
We are going to import angr.
and then, we'll create a project,

14
00:01:18,800 --> 00:01:23,905
angr project and the path

15
00:01:39,600 --> 00:01:48,400
It's good to start having the load options
always set to the auto_load_libs false.

16
00:01:48,400 --> 00:01:51,450
Not that it's completely
necessary in this case,

17
00:01:51,904 --> 00:01:59,405
but it's a good practice if you're going
to use it to create control flow graphs.

18
00:02:00,540 --> 00:02:05,886
So I always have this kind of template.

19
00:02:05,911 --> 00:02:08,984
Let's set it to false.

20
00:02:17,154 --> 00:02:20,409
We can create a simulation,

21
00:02:25,405 --> 00:02:30,910
project, factory,
and then, the simgr.

22
00:02:34,080 --> 00:02:37,760
Next, we'll explore,

23
00:02:45,040 --> 00:02:53,338
and what we want to find this time is the
lambda function that we always have,

24
00:02:53,363 --> 00:02:58,908
but let's try something
just a little bit different....

25
00:03:03,408 --> 00:03:07,050
.....using strings because strings
are not really reversing,

26
00:03:07,075 --> 00:03:12,284
and exercises, A, and Tokyo westerns.

27
00:03:16,155 --> 00:03:18,336
We've been looking to the strings here.

28
00:03:20,409 --> 00:03:25,145
We can see that there is this Rev! Rev!
Rev! from reversing probably,

29
00:03:25,161 --> 00:03:27,064
and then, you'll get the input.

30
00:03:28,080 --> 00:03:31,357
And depending on what we input here.

31
00:03:31,369 --> 00:03:35,587
Probably, we'll have the same as
password ok and password wrong.

32
00:03:35,612 --> 00:03:37,454
We have input error and correct.

33
00:03:37,470 --> 00:03:41,403
So, let's try to find the path
that leads us to correct.

34
00:03:42,480 --> 00:03:44,334
And that's what we are going to do.

35
00:03:44,359 --> 00:03:49,152
We'll create our
exploration and our find value.

36
00:03:49,412 --> 00:03:53,265
In this case, it's a lambda function
that is looking for a string,

37
00:03:53,290 --> 00:03:55,704
and our string in this case is correct.

38
00:03:59,360 --> 00:04:06,293
This should be in the posix dumps,

39
00:04:06,654 --> 00:04:12,059
and this is not the input that we need.

40
00:04:12,907 --> 00:04:16,346
We need this to be in the out.

41
00:04:18,403 --> 00:04:19,809
So, this is how we look.

42
00:04:22,000 --> 00:04:26,568
Angr is going to try to find it.

43
00:04:26,593 --> 00:04:27,888
Still running.

44
00:04:32,904 --> 00:04:38,209
We actually have an active path,
two times deadended,

45
00:04:38,234 --> 00:04:40,142
but we have one found.

46
00:04:40,167 --> 00:04:43,730
So, let's check what was found.

47
00:04:46,040 --> 00:04:49,830
If you can remember,
we can check what's found,

48
00:04:50,159 --> 00:04:52,939
getting all the paths that we have there.

49
00:04:53,407 --> 00:04:54,443
We'll look into what was found.

50
00:04:54,468 --> 00:04:55,101
We have one.

51
00:04:55,126 --> 00:04:56,448
So, that's in the index zero.

52
00:04:57,120 --> 00:05:00,067
We'll look into the posix dumps.

53
00:05:01,153 --> 00:05:03,955
Let's just check what's in the one,

54
00:05:03,980 --> 00:05:07,014
and we actually have it, the correct.

55
00:05:07,039 --> 00:05:10,632
So, let's see what was the input for it.

56
00:05:10,657 --> 00:05:13,482
You can see that the flag is here,

57
00:05:13,507 --> 00:05:18,050
the ff byte, and then,
we have the flags.

58
00:05:18,657 --> 00:05:22,184
So, let's just store this in our flag.

59
00:05:23,840 --> 00:05:33,151
You can do this by
turning it into a string,

60
00:05:39,904 --> 00:05:42,158
and then, we need to remove
the bytes in the beginning.

61
00:05:43,906 --> 00:05:44,781
and in the end

62
00:05:46,158 --> 00:05:47,405
Let's hope it works.

63
00:05:48,909 --> 00:05:49,859
There you go.

64
00:05:49,884 --> 00:05:51,831
So, we have the flag.

