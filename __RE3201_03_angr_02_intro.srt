﻿1
00:00:00,290 --> 00:00:04,380
Finally, we are going to
start to talk about angr.

2
00:00:04,380 --> 00:00:08,460
Angr is way more than a
symbolic execution engine.

3
00:00:08,460 --> 00:00:14,024
It's a whole binary analysis framework
written in Python by the shellfish team

4
00:00:14,049 --> 00:00:19,380
and a lot of other people
that are contributors.

5
00:00:19,380 --> 00:00:21,700
It offers great APIs,

6
00:00:21,700 --> 00:00:27,710
and it has VEX, the Valgrind
intermediate language behind it.

7
00:00:27,710 --> 00:00:30,950
It also uses z3 that we already know,

8
00:00:30,950 --> 00:00:37,690
and z3 is responsible for most of
the reasoning the angr is doing.

9
00:00:37,690 --> 00:00:39,950
Angr is a very complex tool,

10
00:00:39,950 --> 00:00:46,900
but it is still one of
the most easy to use,

11
00:00:46,900 --> 00:00:49,548
and that's why most people are using it,

12
00:00:49,560 --> 00:00:54,079
not only because it's under active
development for a long time.

13
00:00:54,079 --> 00:00:55,714
and the support is great

14
00:00:55,739 --> 00:00:58,667
because you can just ask
people for features,

15
00:00:58,692 --> 00:01:02,800
and months later, you'll have it.

16
00:01:02,801 --> 00:01:05,261
It's very easy to use.

17
00:01:05,261 --> 00:01:08,029
Let's talk a little
bit about VEX.

18
00:01:08,054 --> 00:01:13,430
VEX is an intermediate language,
as I said before, behind angr.

19
00:01:15,655 --> 00:01:19,653
The problem in the past with angr
was that it was using SimuVEX.

20
00:01:19,907 --> 00:01:26,439
It was summarising and abstracting a lot
of different things from the binary,

21
00:01:26,464 --> 00:01:30,159
just elevating it to VEX.

22
00:01:30,160 --> 00:01:37,961
It was very expensive to do
it in a normal processor.

23
00:01:37,961 --> 00:01:49,511
What the team has done in the last
couple of years is to move to PyVex.

24
00:01:49,511 --> 00:01:55,148
PyVex are the Python bindings for
Vex that can be integrated in angr

25
00:01:55,173 --> 00:01:58,170
that is already written in Python.

26
00:01:58,171 --> 00:02:01,801
Since then, control flow
graph generation just works.

27
00:02:01,801 --> 00:02:07,023
It's very fast, and in
comparison to other tools,

28
00:02:07,048 --> 00:02:11,931
it makes sense and it
doesn't break that much.

29
00:02:11,932 --> 00:02:21,810
We are going to solve three very easy
challenges in the next videos using angr,

30
00:02:21,810 --> 00:02:25,724
so that you can get a bit acquainted
with it and how it works,

31
00:02:25,749 --> 00:02:29,049
how the API  looks like.

32
00:02:29,050 --> 00:02:30,630
So, get the VM started.

