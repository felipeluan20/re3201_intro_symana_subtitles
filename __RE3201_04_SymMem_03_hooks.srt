﻿1
00:00:00,099 --> 00:00:07,394
We could have solved this
challenge in a way more simple

2
00:00:07,419 --> 00:00:13,410
and clean using hooks
and SimProcedures.

3
00:00:13,410 --> 00:00:18,049
So, we can use hooks when we want to inject
symbols during execution

4
00:00:18,074 --> 00:00:23,670
like we wanted to do in scanf

5
00:00:23,671 --> 00:00:28,426
So, if you have an infinite loop
or a kind of more complex function,

6
00:00:28,451 --> 00:00:31,903
or like in the moon case

7
00:00:32,405 --> 00:00:33,594
We can use hooks.

8
00:00:34,159 --> 00:00:37,464
In angr, the recipe would be basically,

9
00:00:38,155 --> 00:00:40,348
we'll find a problematic function.

10
00:00:40,373 --> 00:00:47,011
We'll look at its address, and which
address that we want to skip,

11
00:00:47,011 --> 00:00:50,910
and we'll run our own code instead.

12
00:00:50,921 --> 00:00:53,910
Then, we'll check the number
of bytes we want to skip,

13
00:00:54,410 --> 00:00:58,190
which can be zero in the case
that we want to run our own code

14
00:00:58,215 --> 00:01:02,010
in addition to the binary code,

15
00:01:02,010 --> 00:01:05,779
and rewrite a function that
we want to execute

16
00:01:05,804 --> 00:01:09,030
instead of the complex
or one or the syscall

17
00:01:09,031 --> 00:01:12,015
So, basically, it works this way.

18
00:01:12,040 --> 00:01:16,156
We have a program that
is expecting any input.

19
00:01:16,405 --> 00:01:21,571
It has a weird complex function or a syscall

20
00:01:21,572 --> 00:01:27,706
And then, the normal code using the
return of this complex function

21
00:01:27,731 --> 00:01:34,659
is going to be used in a way that is
like a condition for us to get flagz.

22
00:01:36,109 --> 00:01:41,546
So, what we'll do is, firstly,
we'll create a better function

23
00:01:41,558 --> 00:01:50,140
that is going to be used to substitute the
complex function or even overwrite it.

24
00:01:50,140 --> 00:02:01,251
And the hook is going to be exactly right
before the function that we want to hook

25
00:02:01,276 --> 00:02:03,939
or to overiwrite, or to add.

26
00:02:03,939 --> 00:02:12,410
So, the code that we want to skip
or override is going to be jumped

27
00:02:12,410 --> 00:02:18,860
And then, the normal code will be
executed using the return of my function

28
00:02:18,860 --> 00:02:23,350
and not the one from
the complex function.

29
00:02:23,350 --> 00:02:29,703
So, what we'll need to use hooks and SimProcedures
procedures is first the addresses

30
00:02:29,728 --> 00:02:31,821
that we want to hook.

31
00:02:31,821 --> 00:02:35,716
In the moon case is scanf function, for example,

32
00:02:35,741 --> 00:02:39,580
or anywhere that we
want to put our hook.

33
00:02:39,580 --> 00:02:44,238
We need to know how many bytes we want
to skip if we want to skip something,

34
00:02:45,161 --> 00:02:46,950
and then, we'll run.

35
00:02:46,975 --> 00:02:52,071
Also to write this better
function that is Python,

36
00:02:52,096 --> 00:02:57,900
so it should be easy to write.

37
00:02:57,901 --> 00:03:01,153
You could have solved, as I said,
before the moon challenge

38
00:03:01,979 --> 00:03:03,637
using this function,

39
00:03:03,662 --> 00:03:08,530
like this technique using hooks
and creating a sim procedure

40
00:03:08,530 --> 00:03:09,409
It's going to be a homework.

41
00:03:10,530 --> 00:03:15,540
We are going to see some examples of
it when we are solving the binary bomb.

