﻿1
00:00:00,240 --> 00:00:03,338
Now, we are back for phase two.

2
00:00:05,154 --> 00:00:09,402
Where you start as always with the disassemble
to have an idea of,

3
00:00:09,904 --> 00:00:13,122
which kind of workflow we need.

4
00:00:13,147 --> 00:00:20,147
And this time, we have a bit
longer kind of function.

5
00:00:20,172 --> 00:00:26,562
And first, we have this call to
read_six_numbers.

6
00:00:26,587 --> 00:00:32,747
And then, we have explode bomb multiple times,
not only one,

7
00:00:32,772 --> 00:00:41,600
and until we have this return if we
do not jump to this another call.

8
00:00:43,600 --> 00:00:46,410
So, what we are going to do is,

9
00:00:47,157 --> 00:00:50,264
have a look into the read
six numbers function.

10
00:00:51,040 --> 00:00:54,157
So, we are going to
use the lldb command

11
00:00:55,865 --> 00:00:58,905
to disassemble...

12
00:01:01,405 --> 00:01:02,654
...the...

13
00:01:04,903 --> 00:01:06,407
....function.

14
00:01:14,400 --> 00:01:18,659
Read six numbers.

15
00:01:23,651 --> 00:01:28,720
As you can see, something is happening
here that is allocating these six numbers.

16
00:01:29,680 --> 00:01:34,251
So, probably, it's expecting
us to input six numbers,

17
00:01:34,904 --> 00:01:41,625
and if some of the checks here are
not okay, it's going to call our bomb.

18
00:01:42,410 --> 00:01:50,857
So, this time, we get to use the hooks
that we just had in the last session.

19
00:01:50,882 --> 00:01:55,260
And what we'll do here is
create a SimProcedure.

20
00:01:55,285 --> 00:02:01,906
As I said before, these are kind of classes
with function that is self-defined,

21
00:02:02,654 --> 00:02:10,656
and it is going to be added or overwrite
some other functions inside the binary.

22
00:02:11,903 --> 00:02:18,459
So, what we are going to do is to
substitute this read_six_numbers function.

23
00:02:18,484 --> 00:02:22,882
We have our own six
numbers kind of function

24
00:02:22,907 --> 00:02:27,523
that is going to be used
probably into phase two.

25
00:02:27,548 --> 00:02:30,068
So, we have this read_six_numbers

26
00:02:30,080 --> 00:02:33,760
that is going to define
some kind of structure

27
00:02:33,760 --> 00:02:37,151
that is going to be further used here.

28
00:02:37,176 --> 00:02:41,152
So, in our read_six_numbers_function,

29
00:02:41,411 --> 00:02:43,657
we are going to have Ints,

30
00:02:44,659 --> 00:02:49,162
And these Ints are going to be
bit vectors that are symbolic,

31
00:02:49,653 --> 00:02:54,926
because we don't know which
kind of numbers we have, right?

32
00:02:54,951 --> 00:02:58,904
And these are going to
be an array with six.

33
00:03:00,240 --> 00:03:02,581
And you can return anything you want.

34
00:03:02,606 --> 00:03:04,405
Just returning two because
it's the phase two.

35
00:03:05,659 --> 00:03:07,522
So, after defining this function,

36
00:03:07,547 --> 00:03:14,320
what we can do is try to define the
relevant addresses as I said before,

37
00:03:15,760 --> 00:03:20,151
So, now that we have our "better" function,

38
00:03:20,176 --> 00:03:25,905
Again, we need to find
the phase two address,

39
00:03:26,410 --> 00:03:28,405
and this is going to
be the base address.

40
00:03:28,960 --> 00:03:32,029
Again, close to the call to phase two.

41
00:03:32,054 --> 00:03:36,404
So, we can go back to the main function

42
00:03:38,560 --> 00:03:44,980
and see that the phase two is
called in the offset 0x14c6.

43
00:03:45,005 --> 00:03:47,937
So, that's what we are going to use.

44
00:03:52,972 --> 00:03:55,723
So, this is this going
to be base address,

45
00:03:57,104 --> 00:04:00,469
0x14c6.

46
00:04:03,972 --> 00:04:09,722
We also need the address of the
function that we want to hook.

47
00:04:09,975 --> 00:04:13,224
So, this is going to be the
read_six_numbers address,

48
00:04:14,221 --> 00:04:16,452
and this is base address.

49
00:04:16,477 --> 00:04:22,725
Plus the offset that we
can see in the call here.

50
00:04:23,468 --> 00:04:26,975
So, here we go.

51
00:04:27,468 --> 00:04:29,720
It's 0x1c11.

52
00:04:30,221 --> 00:04:33,997
This is the read_six_numbers function.

53
00:04:34,022 --> 00:04:46,220
So, we can go here and
have the address 0x1c11.

54
00:04:47,472 --> 00:04:50,887
We also need a target for angr.

55
00:04:50,912 --> 00:04:55,725
And what's important to
know here in this case is

56
00:04:56,473 --> 00:04:57,738
we need two targets,

57
00:04:57,763 --> 00:05:02,970
because we have here our explode
bomb inside the read_six_numbers,

58
00:05:03,300 --> 00:05:11,719
and we have explode bomb also
in the phase two function.

59
00:05:12,223 --> 00:05:15,718
So, what we are going to
do is to define first,

60
00:05:16,968 --> 00:05:19,723
target for the function 2A

61
00:05:20,970 --> 00:05:24,459
that is going to be base address,

62
00:05:24,970 --> 00:05:25,469
plus,

63
00:05:26,471 --> 00:05:34,722
and then, we can go to phase two and
see which address do we want to reach.

64
00:05:35,719 --> 00:05:45,472
And this would be, first, we want to
come back from the read_six_numbers

65
00:05:46,470 --> 00:05:53,978
and not jump to the 0x15fd because
this would be the explode bomb.

66
00:05:54,470 --> 00:05:57,224
So, this is the first
explode bomb that we have.

67
00:05:57,600 --> 00:06:02,721
So, what we want is one line after 0x15fd.

68
00:06:02,800 --> 00:06:06,475
So, this is going to
be our first target.

69
00:06:07,650 --> 00:06:12,975
So, base adress 0x15f3,

70
00:06:14,722 --> 00:06:22,220
and the second one, the target_2_B
is going to be base address,

71
00:06:23,970 --> 00:06:28,276
and we want something after now,

72
00:06:28,301 --> 00:06:35,686
like this time, we avoided the
read_six_numbers explode bomb,

73
00:06:35,711 --> 00:06:42,352
and we want to go to the end
and this would be a return.

74
00:06:42,605 --> 00:06:45,358
So, we really want to reach the return,

75
00:06:45,500 --> 00:06:49,609
so we can defuse the
bomb and this is 0x1633.

76
00:06:50,859 --> 00:06:52,608
This is our final goal.

77
00:06:55,280 --> 00:07:00,920
So, we can set here for 0x1633.

78
00:07:01,200 --> 00:07:08,400
So, now we have everything we need
for our recipe or using hooks.

79
00:07:09,668 --> 00:07:15,128
Now, we can add our hook
and this is very simple.

80
00:07:15,168 --> 00:07:22,837
We had hook for project and we
are hooking the read_six_numbers,

81
00:07:24,920 --> 00:07:33,920
and we are going to substitute this
with the read_six_numbers function.

82
00:07:37,040 --> 00:07:39,900
So, it's hooked now.

83
00:07:39,925 --> 00:07:45,072
We can actually run our simulation.

84
00:07:45,097 --> 00:07:48,528
we'll do it as we already know.

85
00:07:49,360 --> 00:07:55,923
We have a state and this is
going to be the blank state again.

86
00:08:02,800 --> 00:08:11,419
and we are going to use
as an address, phase two.

87
00:08:12,420 --> 00:08:17,894
We are going to use something
called remove options

88
00:08:18,422 --> 00:08:22,000
and we are going to remove the lazy solvers

89
00:08:22,000 --> 00:08:23,886
I can explain it later.

90
00:08:23,911 --> 00:08:27,304
It's just because it takes
too long otherwise.

91
00:08:40,240 --> 00:08:46,419
Now that we have a state,
we can start our simulation manager.

92
00:09:00,160 --> 00:09:04,635
Add our state that we just created,

93
00:09:04,660 --> 00:09:14,419
and we are going to let it run for
as long as we need to get an answer.

94
00:09:14,430 --> 00:09:18,306
So, we can do just like we had before.

95
00:09:19,133 --> 00:09:24,169
We'll run this as long as
there's a lot of active

96
00:09:31,806 --> 00:09:33,919
states

97
00:09:34,165 --> 00:09:41,141
and we'll try to find the first target.

98
00:09:41,165 --> 00:09:55,671
So, we can use explore, and we are
going to use as a find the target_2_A

99
00:09:59,440 --> 00:10:01,417
We are going to avoid the bomb.

100
00:10:06,921 --> 00:10:10,109
So, running it is going
to take a while again.

101
00:10:12,671 --> 00:10:13,668
Worry not!

102
00:10:13,700 --> 00:10:15,920
We're going to cut it off.

103
00:10:16,425 --> 00:10:18,420
So, we're back,

104
00:10:18,500 --> 00:10:22,308
and let's see what angr found.

105
00:10:24,257 --> 00:10:26,928
You're going to analyze now,

106
00:10:26,953 --> 00:10:33,243
all of the states that were
found during the first search.

107
00:10:33,268 --> 00:10:37,505
For each one of them,
we are going to simulate again

108
00:10:37,700 --> 00:10:44,253
which of the first founds will
lead us to the final return.

109
00:10:45,505 --> 00:10:52,253
To do that, or you can just iterate
through the states angr found before.

110
00:10:52,700 --> 00:10:58,254
So first, we'll create
a new simulation,

111
00:11:11,280 --> 00:11:17,756
and we'll use the state
that we have in founds

112
00:11:22,260 --> 00:11:24,260
and then, we'll explore.

113
00:11:25,226 --> 00:11:32,755
The new exploration is going to
try to find the second target,

114
00:11:33,008 --> 00:11:35,754
target_2_B,

115
00:11:37,200 --> 00:11:46,397
and of course, it's going to avoid
the bomb and if we find something.

116
00:11:46,422 --> 00:11:59,253
So, in this case there are some states that angr
classifies it as found.

117
00:11:59,757 --> 00:12:03,624
We should have our flag.

118
00:12:10,240 --> 00:12:15,564
Let's run this and search,
and this one's very fast.

119
00:12:16,756 --> 00:12:19,510
What we need to do now
is get our values.

120
00:12:20,007 --> 00:12:23,958
So, this is going to be
the flag number two,

121
00:12:23,983 --> 00:12:28,436
and we are going to do
solver.eval as we already know,

122
00:12:29,006 --> 00:12:35,004
and we want to add all the ints that
we found like these six numbers, right?

123
00:12:42,560 --> 00:12:46,800
Maybe I can just print the flag,
so I have an idea what we got.

124
00:12:51,680 --> 00:12:57,850
So, as you can see, we have six ints.

125
00:12:57,875 --> 00:12:58,509
We already stored it

126
00:12:58,600 --> 00:13:03,754
So, let's store it in our file,

127
00:13:07,257 --> 00:13:11,754
and again, we can test
it with our binary,

128
00:13:13,723 --> 00:13:18,005
just to be sure we
have the right flag.

129
00:13:31,120 --> 00:13:33,840
Exercises, 0C, bomb. and
bomb, flags, binary.

130
00:13:46,800 --> 00:13:50,219
and as you see, we get again

131
00:13:50,244 --> 00:13:55,224
The first one that we
diffused and the second one.

