﻿1
00:00:00,160 --> 00:00:05,212
We are going to solve the
binary bomb only using angr.

2
00:00:05,237 --> 00:00:10,213
The binary bomb should be
already known to everybody

3
00:00:10,238 --> 00:00:13,969
that is doing the
reverse engineering path

4
00:00:13,994 --> 00:00:16,904
It's a very well known challenge.

5
00:00:17,680 --> 00:00:21,472
So, you're going to
start this every time.

6
00:00:21,497 --> 00:00:25,024
Firstly, we are going
to load lldb,

7
00:00:29,440 --> 00:00:33,432
and then, I have my normal
reverse engineering flow.

8
00:00:33,908 --> 00:00:36,257
I'm looking at the
kind of binary I have.

9
00:00:36,282 --> 00:00:38,622
I have a binary 64-bit.

10
00:00:38,647 --> 00:00:43,176
So, I'm going to just
check the strings.

11
00:00:44,407 --> 00:00:48,655
As we know, we can use an input
file to have all the flags

12
00:00:48,907 --> 00:00:50,879
in the case of the binary bomb,

13
00:00:50,904 --> 00:00:53,280
and then, we have the strings

14
00:00:53,281 --> 00:00:57,034
telling us that we were
successful by each phase,

15
00:00:57,059 --> 00:00:59,385
like phase diffused, and so on.

16
00:01:00,320 --> 00:01:05,660
We also have a string telling
us that the bomb has blown up,

17
00:01:05,685 --> 00:01:07,440
and we lost it.

18
00:01:07,440 --> 00:01:11,280
It's interesting to see that we also
have like some kind of string format

19
00:01:11,657 --> 00:01:12,584
going on here.

20
00:01:14,640 --> 00:01:21,025
And of course, if you want to know
which version of the binary bomb

21
00:01:21,050 --> 00:01:23,560
we're going to solve here,

22
00:01:23,585 --> 00:01:29,360
we can always have a look into the strings
of the service that we have here.

23
00:01:31,600 --> 00:01:39,840
But let's start with loading our binary
into lldb and checking the main function.

24
00:01:41,920 --> 00:01:46,404
We can see the main function
of the binary bomb is

25
00:01:46,429 --> 00:01:49,944
getting an input from the user,
like from us,

26
00:01:50,800 --> 00:01:57,152
and calling the phase using that input

27
00:01:57,908 --> 00:02:01,544
that we passed to the main
function to the phase.

28
00:02:02,400 --> 00:02:06,664
And every time it returns
from the phase function,

29
00:02:06,676 --> 00:02:11,040
it's going to defuse and
lead us to the next phase.

30
00:02:13,520 --> 00:02:16,735
That's basically what has
happened in the main function.

31
00:02:16,760 --> 00:02:20,274
So, let's have a look into
the phase number one,

32
00:02:20,299 --> 00:02:25,440
and then, we can see that the phase
number one is pretty simple.

33
00:02:25,441 --> 00:02:28,072
It's calling a function
named strings_not_equal.

34
00:02:28,097 --> 00:02:31,100
We can assume it's a string compare.

35
00:02:31,125 --> 00:02:33,369
We don't need to really check it

36
00:02:33,394 --> 00:02:37,688
because we are going to solve ir with angr
what it's basically doing,

37
00:02:38,405 --> 00:02:43,189
it's taking the input that we
had and calling this function,

38
00:02:43,201 --> 00:02:46,057
and if these things are not equal,

39
00:02:46,082 --> 00:02:50,456
it's going to jump to the
explode bomb function.

40
00:02:51,407 --> 00:02:57,545
So, if these things are equal, it's going
to come here to the ??? pointer,

41
00:02:57,570 --> 00:02:58,851
and then, return.

42
00:02:58,876 --> 00:03:00,668
That's what we want, right?

43
00:03:00,685 --> 00:03:03,944
We want to return to
get the phase diffused

44
00:03:04,880 --> 00:03:06,717
and go to the second phase.

45
00:03:06,742 --> 00:03:10,800
So, what we are going to do is
first load angr and claripy

46
00:03:10,800 --> 00:03:13,913
because we are going to
solve all the phases in this

47
00:03:13,938 --> 00:03:16,221
document or notebook,

48
00:03:16,246 --> 00:03:18,985
and then, we'll load our binary.

49
00:03:20,157 --> 00:03:22,871
Something that I haven't done before,

50
00:03:22,896 --> 00:03:28,080
but it's a good thing to start now.

51
00:03:28,720 --> 00:03:32,264
We don't want to hardcode
all the addresses.

52
00:03:32,276 --> 00:03:35,600
So, for not doing that,
we can use a loader,

53
00:03:36,160 --> 00:03:38,020
and then, it's the main binary.

54
00:03:38,405 --> 00:03:43,010
So, it's our main object and minimal
address is going to be our base address.

55
00:03:43,035 --> 00:03:46,609
So, this way, we don't need to hardcode
the base address.

56
00:03:46,634 --> 00:03:51,473
The bomb is going to be
our always avoid address,

57
00:03:51,498 --> 00:04:02,408
this is the base address plus the offset 0x1be5,
as we can see in the phase one.

58
00:04:03,072 --> 00:04:06,960
So, let's set this because we are
going to use it in every phase,

59
00:04:07,600 --> 00:04:09,929
and it started phase one.

60
00:04:09,954 --> 00:04:14,984
To start with phase one,
let's disassemble it first.

61
00:04:17,760 --> 00:04:24,109
Looking at it, we know that it's
doing a string compare probably,

62
00:04:24,134 --> 00:04:27,040
and if everything is alright,

63
00:04:27,040 --> 00:04:28,718
we are going to return.

64
00:04:28,743 --> 00:04:30,142
So, what do we want?

65
00:04:30,167 --> 00:04:33,920
First of all, we need the
base address of phase one,

66
00:04:33,921 --> 00:04:38,097
and this is where phase one is
called from the main function.

67
00:04:38,122 --> 00:04:40,921
As we can see, it's 0x15a7,

68
00:04:42,090 --> 00:04:45,400
our call, so we go to the main function

69
00:04:45,412 --> 00:04:52,502
and see where they call 0x15a7,
phase one has been done,

70
00:04:52,527 --> 00:04:56,371
and it's in the 0x14a8 offset.

71
00:04:56,396 --> 00:04:59,127
Go back to our code

72
00:04:59,587 --> 00:05:06,338
and we defined that the
phase one call is in 0x14a8,

73
00:05:07,087 --> 00:05:08,271
plus the base address.

74
00:05:08,296 --> 00:05:15,339
And our target we want to
reach or our find address,

75
00:05:15,588 --> 00:05:18,113
as we have been valling it before,

76
00:05:18,138 --> 00:05:22,763
is going to be this line
after the test and the jump.

77
00:05:22,788 --> 00:05:25,785
So, we want that the
jump is not taken.

78
00:05:25,810 --> 00:05:28,321
We want to reach this address.

79
00:05:28,321 --> 00:05:31,955
That's what you're going to use
here as a target for angr.

80
00:05:31,966 --> 00:05:35,760
And we are going to
have a similar workflow

81
00:05:36,960 --> 00:05:41,984
as we had for the most
simple exercises until now

82
00:05:42,009 --> 00:05:49,639
Just creating a state that it's starting
with the base address from phase one.

83
00:05:49,651 --> 00:05:54,290
and then, we'll start a
simulation manager with this state

84
00:05:54,315 --> 00:05:59,339
And we let angr explore,
trying to find the target address

85
00:05:59,587 --> 00:06:00,904
and avoiding the bomb.

86
00:06:03,280 --> 00:06:06,543
What is interesting
to see here is that

87
00:06:06,568 --> 00:06:12,359
a simple thing as a string compare
that for most of reverse engineers,

88
00:06:12,383 --> 00:06:15,808
quite simple is to
reverse and understand,

89
00:06:16,560 --> 00:06:21,300
is one of the functions that takes
most time to calculate for angr,

90
00:06:22,085 --> 00:06:25,600
because it's going to
test all possible inputs.

91
00:06:25,625 --> 00:06:31,440
And as we haven't restricted or
constraint our input in this way,

92
00:06:32,240 --> 00:06:39,838
it takes a long time, but I'm
going to cut in the video,

93
00:06:40,092 --> 00:06:41,224
So, don't wait for it.

94
00:06:43,120 --> 00:06:44,785
We're back.

95
00:06:44,810 --> 00:06:52,024
We finally have something and I'm just
going to scroll down as you can see.

96
00:06:58,800 --> 00:07:00,648
And there we go.

97
00:07:00,673 --> 00:07:02,674
Our simulation found one.

98
00:07:02,699 --> 00:07:07,935
I needed to avoid 129 different paths.

99
00:07:07,960 --> 00:07:10,455
So, let's get a flag now.

100
00:07:11,756 --> 00:07:17,064
We are going to get the memory
address where the flag is stored,

101
00:07:17,760 --> 00:07:21,911
and then, we need to
evaluate that or concretize that.

102
00:07:21,923 --> 00:07:26,000
So, for that we already have a known
workflow

103
00:07:26,000 --> 00:07:29,670
We are going to restore the found state,

104
00:07:29,682 --> 00:07:34,046
and from this found state,
we are going to get the register rsi.

105
00:07:34,071 --> 00:07:41,704
If you can remember, in the binary,
the input that is going to be used

106
00:07:43,120 --> 00:07:51,280
on strings not equal is rsi since
we are in the 64 architecture,

107
00:07:53,752 --> 00:07:55,507
and....

108
00:07:57,008 --> 00:08:06,640
what we are going to do is get the address
that it is stored in that memory.

109
00:08:06,640 --> 00:08:12,134
We are going to pass it to the
solver evaluate and cast it to bytes,

110
00:08:12,159 --> 00:08:14,824
as we already know how it works.

111
00:08:15,600 --> 00:08:22,761
As we can see, the strings that
lead us to the found path is

112
00:08:23,007 --> 00:08:26,080
'I am just a renegade hockey mom.'

113
00:08:26,080 --> 00:08:30,537
So, what we are going to do is to
store this string into a file,

114
00:08:30,562 --> 00:08:32,344
because as we saw before,

115
00:08:33,120 --> 00:08:37,320
we can just pass a file with
all the flags to the binary.

116
00:08:37,345 --> 00:08:43,008
So, you're going to store this
into the bomb_flags.txt,

117
00:08:43,505 --> 00:08:45,304
and then, we can test our flag.

118
00:08:47,280 --> 00:08:52,562
As we can see, we use these
strings for phase one,

119
00:08:53,509 --> 00:08:57,237
and we get phase one diffused.

120
00:08:57,262 --> 00:09:03,210
I'm going to kill the process because it's
expecting the flag for phase number two,

121
00:09:03,235 --> 00:09:05,384
and that's in the next video.

