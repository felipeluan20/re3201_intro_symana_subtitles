﻿1
00:00:00,407 --> 00:00:04,779
For this one, you have all the tools
that you need to solve it by yourself.

2
00:00:04,779 --> 00:00:08,597
So, I will just walk through
it for completeness,

3
00:00:08,609 --> 00:00:12,200
so that you have an
overview of how it works,

4
00:00:12,200 --> 00:00:18,510
and it's just like a process that you
need to familiarise yourself with.

5
00:00:18,510 --> 00:00:22,230
Firstly, I will have
my debugger on,

6
00:00:22,255 --> 00:00:27,020
and then, I'll do file to check
which kind of binary I have.

7
00:00:27,020 --> 00:00:30,927
So, here it is again, ELF 32-bit binary,

8
00:00:30,952 --> 00:00:32,730
I'll do the strings,

9
00:00:32,731 --> 00:00:36,007
and this time, it's a
pretty similar structure.

10
00:00:36,032 --> 00:00:40,016
As you can see, I also have
the IOLI crackme level here,

11
00:00:40,041 --> 00:00:41,344
a string here,

12
00:00:41,369 --> 00:00:47,219
but I also have two strings
that are not even readable,

13
00:00:47,219 --> 00:00:52,616
and then, I have then again
whole GLIBC imports.

14
00:00:52,641 --> 00:01:00,233
So, let's set up the debugger
for the crackme number three,

15
00:01:00,258 --> 00:01:03,181
and disassemble the main.

16
00:01:04,906 --> 00:01:10,404
This time, we can see that the main
function is very similar to the last one again.

17
00:01:11,585 --> 00:01:14,853
You have two printfs  and then,
you have a scanf.

18
00:01:14,878 --> 00:01:18,111
So, we'll scroll down
looking for the compare

19
00:01:18,136 --> 00:01:20,500
but this time, there is no compare.

20
00:01:20,501 --> 00:01:25,791
What we can see here is a call
to a function named test.

21
00:01:25,791 --> 00:01:30,681
So, let's disassemble the test
function and see what it's doing.

22
00:01:31,907 --> 00:01:34,680
So, the test function has a compare,

23
00:01:34,705 --> 00:01:39,646
and if the compare matches with this
thing that they are looking for,

24
00:01:39,671 --> 00:01:46,012
we are going to jump to the plus 28.

25
00:01:47,155 --> 00:01:51,307
So, let's check what it is in there.

26
00:01:51,332 --> 00:02:01,140
So, we have the second weird string that we
saw on the list from the strings.

27
00:02:01,141 --> 00:02:05,200
So, we don't understand completely
what this binary is doing,

28
00:02:05,225 --> 00:02:07,130
but the structure is similar.

29
00:02:07,130 --> 00:02:11,122
So, let's try to solve
it with angr anyway.

30
00:02:11,147 --> 00:02:12,706
We'll import angr.

31
00:02:12,731 --> 00:02:14,618
We'll start a project

32
00:02:20,405 --> 00:02:29,430
This time, with the binary
crackme number three.

33
00:02:29,430 --> 00:02:40,879
And as you remember here,
we just created a simulation,

34
00:02:45,403 --> 00:02:50,654
and we are going to run the
same way we ran the one before.

35
00:02:50,655 --> 00:02:56,654
So, we are going to run until
we find a branching point.

36
00:02:57,951 --> 00:02:59,610
And for finding branching points,

37
00:02:59,635 --> 00:03:01,158
we know we need to
check that the length....

38
00:03:05,606 --> 00:03:14,405
......of all the possible paths that we
have that are active are more than one.

39
00:03:17,655 --> 00:03:24,090
Angr does its magic and
finds two active paths.

40
00:03:24,090 --> 00:03:30,653
So, we are going to do the same and
just look for the kind of input

41
00:03:32,910 --> 00:03:36,481
that would lead us to would lead
us to each of the possible paths.

42
00:03:36,481 --> 00:03:46,740
And again, we have one int
and some kind of weird int.

43
00:03:46,740 --> 00:03:50,371
So, we'll start the first one,

44
00:03:51,155 --> 00:03:53,340
and that's already the right one.

